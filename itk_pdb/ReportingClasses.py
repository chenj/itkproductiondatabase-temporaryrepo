#!/usr/bin/env python
# ReportingClasses.py
# Created: 2019/04/01, Updated: 2019/07/04
# Contributing: MJB

import re, sys
from functools import reduce
from time import strftime
import numpy as np
import matplotlib.pyplot as plt

#######################################################################################################################################################################################################

# -------------------------
# Printing helper functions
# -------------------------

# Pretty colours
class Colours():
    White       = '\033[95m'
    Blue        = '\033[94m'
    Green       = '\033[92m'
    Yellow      = '\033[93m'
    Red         = '\033[91m'
    End         = '\033[0m'
    Bold        = '\033[1m'
    Underline   = '\033[4m'

# Define some pretty printing functions
# Here, they are all included in a single class so we can implement inheritance
# Note: sys._getframe(1) is for fetching (the name of) the function calling the print statement
# NOT ACTUALLY USED ANYWHERE
class Printer(object):

    def __init__(self):
        super(Printer, self).__init__()

    def INFO(self, string):
        print('\033[1m\033[97mINFO\033[0m in %s.%s(...) : %s' % (self.__class__.__name__, sys._getframe(1).f_code.co_name, string))

    def WARNING(self, string):
        print('\033[1m\033[93mWARNING\033[0m in %s.%s(...) : %s' % (self.__class__.__name__, sys._getframe(1).f_code.co_name, string))

    def ERROR(self, string):
        print('\033[1m\033[91mERROR\033[0m in %s.%s(...) : %s' % (self.__class__.__name__, sys._getframe(1).f_code.co_name, string))

    def BREAK(self, string):
        print('\033[1m\033[91mBREAK\033[0m in %s.%s(...) : %s' % (self.__class__.__name__, sys._getframe(1).f_code.co_name, string))

# Define a handy function for logging
# Note: this should be the default for printing/logging during reporting
# NOTE TO MATT (USEFUL): https://stackoverflow.com/questions/21105753/python-logger-not-working
import logging
def getLogger(name, show_in_console = True, sh_level = logging.INFO, logfile = None, fh_level = logging.INFO, overwrite = False):
    logger = logging.getLogger(name)
    if show_in_console and logfile:
        if sh_level < fh_level:
            base_level = sh_level
        else:
            base_level = fh_level
    elif show_in_console:
        base_level = sh_level
    else:
        base_level = fh_level
    logger.setLevel(base_level)
    formatter = logging.Formatter('[%(filename)s:%(funcName)-15s:%(lineno)-3s] %(asctime)s : %(name)s : %(levelname)-8s : %(message)s')
    if show_in_console:
        sh = logging.StreamHandler(sys.stdout)
        sh.setFormatter(formatter)
        sh.setLevel(sh_level)
        logger.addHandler(sh)
    # If we are writing out a logfile
    if logfile:
        if os.path.exists(logfile):
            if overwrite:
                print('%s%sWARNING%s in getLogger(...) : Log file \'%s\' already exists, but it will be overwritten.' % (Colours.Bold, Colours.Yellow, logfile, Colours.End))
                mode = 'w'
            else:
                print('%s%sERROR%s in getLogger(...) : Log file \'%s\' already exists, and it will NOT be overwritten.' % (Colours.Bold, Colours.Red, logfile, Colours.End))
                raise Exception
        else:
            pass
            mode = 'w'
        # Add the logfile handler
        fh = logging.FileHandler(filename = logfile, mode = mode)
        fh.setFormatter(formatter)
        fh.setLevel(fh_level)
        logger.addHandler(fh)
    if not (show_in_console or logfile):
        print('%s%sWARNING%s in getLogger(...) : Logging is not being dumped to the console or a logfile -- is this intended?' % (Colours.Bold, Colours.Yellow, Colours.End))
    return logger

# Define our default logger, to be used globally in the file
_LOGGER_DEFAULT_LEVEL = logging.ERROR
_LOGGER_DEFAULT = getLogger('default', show_in_console = True, sh_level = _LOGGER_DEFAULT_LEVEL)

#######################################################################################################################################################################################################

# ------------------------------------
# Base functionality for applying cuts
# ------------------------------------

# Define a cut class with a Cut.apply__BASE(...) function defined during instantiation
# We also define a global list of allowed operators
# The individual operator cases must be handled in Cut.__init__
_ALLOWED_OPERATORS = ['<', '<=', '=<', '>', '>=', '=>', '==', '=', '!=', 'IN', 'XIN', 'RE', 'TRUE', 'FALSE', 'DUMMY', 'FALSE']

class Cut(object):

    __slots__ = ['name', 'operator', 'cut_value', 'apply__BASE', 'parent', 'logger']

    def __init__(self, name, operator, cut_value = None, parent = None):
        super(Cut, self).__init__()
        self.name       = name
        self.operator   = operator
        self.cut_value  = cut_value
        # Check for the parent and add it
        self.parent     = None
        self.logger     = _LOGGER_DEFAULT
        self.addParent(parent)
        # Have to go through and manually add the filter functions associated with each operator
        # Maybe we could have G_ALLOWED_OPERATORS map operator keys to the functions, though then the cut value
        # would also need to be included as an argument...
        if operator == '<':
            self.apply__BASE = lambda value: value < cut_value
        elif operator == '<=' or self.operator == '=<':
            self.apply__BASE = lambda value: value <= cut_value
        elif operator == '>':
            self.apply__BASE = lambda value: value > cut_value
        elif operator == '>=' or self.operator == '=>':
            self.apply__BASE = lambda value: value >= cut_value
        elif operator == '==' or self.operator == '=':
            self.apply__BASE = lambda value: value == cut_value
        elif operator == '!=':
            self.apply__BASE = lambda value: value != cut_value
        elif operator == 'IN':
            self.apply__BASE = lambda value: value in list(cut_value)
        elif operator == 'XIN':
            self.apply__BASE = lambda value: value not in list(cut_value)
        elif operator == 'RE':
            self.apply__BASE = lambda value: bool(re.search(str(cut_value), str(value)))
        elif operator == 'TRUE':
            self.apply__BASE = lambda value: bool(value)
        elif operator == 'FALSE':
            self.apply__BASE = lambda value: not bool(value)
        elif operator == 'DUMMY':
            self.apply__BASE = lambda value: True
        elif operator == 'XDUMMY':
            self.apply__BASE = lambda value: False
        else:
            # It's possible that the operator is in _ALLOWED_OPERATORS but isn't handled above so we get to this point
            # Again, probably a better of handling things, but this works for now
            self.logger.error('Operator for %s \'%s\' must be one of %s: operator = %s' % (self.__class__.__name__, name, _ALLOWED_OPERATORS, operator))
            raise Exception
        self.logger.debug('Instantiated %s \'%s\': operator = \'%s\', cut_value = %s' % (self.__class__.__name__, name, operator, cut_value))

    def __str__(self):
        return self.name

    def getLongName(self):
        return '%s \'%s\' (%s %s)' % (self.__class__.__name__, self.name, self.operator, self.cut_value)

    def addParent(self, parent):
        self.parent = parent
        self.logger = parent.logger

#######################################################################################################################################################################################################

# ----------------------------------------------------
# Functionality for cutting on (lists of) dictionaries
# ----------------------------------------------------

# For lists of components/test runs from the ITkPD, we define a list of conditionals
# These are keys in the dicts which we want to apply cuts to (i.e., conditional cuts) before applying other cuts (i.e., follower cuts)
# --> We use regexes here for matching
_CONDITIONAL_KEYS =     [   r'$.*\.code',
                            r'$.*\.id',
                            r'$.*\.name'
                        ]

# We define the keys in a general component/test run dict which point to lists within the larger dictionary structure
_LIST_CUTS =    {   'attachments':          r'^attachments\..*',
                    'children':             r'^children\..*',
                    'children.properties':  r'^children\.properties\..*',
                    'comments':             r'^comments\..*',
                    'defects':              r'^defects\..*',
                    'grades':               r'^grades\..*',
                    'parents':              r'^parents\..*',
                    'properties':           r'^properties\..*',
                    'results':              r'^results\..*',
                    'stages':               r'^stages\..*',
                    'tests':                r'^tests\..*',
                    'tests.testRuns':       r'^tests\.testRuns\..*'
                }

# Now we define a class for applying a cut on a dictionary
# --> Inherits from Cut
class DictCut(Cut):

    __slots__ = ['pass_status', 'conditional', 'keys']

    def __init__(self, name, operator, cut_value = None, pass_status = 1, parent = None):
        super(DictCut, self).__init__(name, operator, cut_value, parent)
        # Insist the pass status is an integer
        # Note: the pass status indicates to a parent ListCut the number of cuts which must pass (or fail) in a list to pass the overall ListCut
        if isinstance(pass_status, int):
            self.logger.debug('Pass status for DictCut \'%s\' set to: %s' % (name, pass_status))
        else:
            self.logger.error('Pass status for DictCut \'%s\' must be an integer: tyep(pass_status) = %s' % (name, pass_status))
            raise Exception
        # Cuts have an int pass status that tells them whether it must pass for at least x (if x > 0) items, all items (if x == 0), or fail no more than |x| (if x < 0)
        self.pass_status    = pass_status
        # Determine if this cuts fits one of our conditional tags
        self.conditional    = bool(re.search(r'[' + r'|'.join(_CONDITIONAL_KEYS) + r']', self.name))
        # Keys that must be sequentially applied to the dictionary to get the value to cut one
        self.keys           = self.name.split('.')

    # Return the name, operator, and cut value for the cut
    def representation(self, indent = 0):
        string = indent * ' ' + self.getLongName() + '\n'
        return string

    # We want to remove part of our set of keys if the cut is applied as part of a ListCut
    # --> Parent ListCut applies these already
    def reduceKeys(self, prefix_to_remove):
        if prefix_to_remove[-1] != '.':
            prefix_to_remove += '.'
        self.keys = self.name.replace(prefix_to_remove, '').split('.')
        self.logger.debug('Keys for DictCut \'%s\': %s' % (self.name, self.keys))

    # Reset the keys
    def resetKeys(self):
        self.keys = self.name.split('.')
        self.logger.debug('Keys for DictCut \'%s\': %s' % (self.name, self.keys))

    # Sequentially apply the keys to a dictionary to get the value to cut on, then applies Cut.apply(...)
    def apply(self, dict0):
        result = super(DictCut, self).apply__BASE(self.getValue(dict0, self.keys))
        self.logger.debug('Result for DictCut \'%s\': %s' % (self.name, result))
        return result

    # Sequentially apply the keys to a dictionary to get a value
    def getValue(self, dict0, keys):
        value = reduce(lambda d, k: d.get(k) if d else None, keys, dict0)
        self.logger.debug('Value for DictCut \'%s\': %s' % (self.name, value))
        return value

# We define an object for applying DictCuts to lists
class ListCut(object):

    __slots__ = ['name', 'pass_status', 'keys', 'conditional_cuts', 'follower_cuts', 'apply', 'setPassReq', 'parent', 'logger', 'iter_index']

    def __init__(self, name, pass_status = 1, parent = None):
        super(ListCut, self).__init__()
        self.name               = name
        self.pass_status        = pass_status
        self.keys               = self.name.split('.')
        # Check for the parent and add it
        self.parent             = None
        self.logger             = _LOGGER_DEFAULT
        self.addParent(parent)
        self.logger.debug('Instantiated ListCut \'%s\'.' % name)
        # We have a cuts classified as conditional
        self.conditional_cuts   = []
        # ...and those classified as not conditional
        self.follower_cuts      = []
        # Default value for our setPassReq(...) and apply(...) functions
        self.setPassReq         = lambda list0: self.pass_status
        self.apply              = self.__apply__PASSED
        self.iter_index         = 0

    def __str__(self):
        return self.name

    def __len__(self):
        return len(self.conditional_cuts + self.follower_cuts)

    def __getitem__(self, i):
        return (self.conditional_cuts + self.follower_cuts)[i]

    def __iter__(self):
        self.iter_index = 0
        return self

    def __next__(self):
        if self.iter_index <= (len(self) - 1):
            cut = self[self.iter_index]
            self.iter_index += 1
            return cut
        else:
            raise StopIteration

    # Python 2 iterator fix, see: https://stackoverflow.com/questions/29578469/how-to-make-an-object-both-a-python2-and-python3-iterator
    next = __next__

    # Same as addCut
    def __call__(self, cut):
        self.addCut(cut)

    def getLongName(self):
        return 'ListCut \'%s\' (pass_status = %s)' % (self.name, self.pass_status)

    def getChildCutNames(self):
        return [cut.name for cut in self]

    def addParent(self, parent):
        self.parent = parent
        # Use the parent's logger
        self.logger = parent.logger

    # Return the name of the cut, its pass status, and all contained cuts (with nice indenting)
    def representation(self, indent = 0):
        string = indent * ' ' + self.getLongName() + '\n'
        for i in range(len(self)):
            string += self[i].representation(indent = indent + 1)
        return string

    # Same as for DictCut
    def reduceKeys(self, prefix_to_remove):
        if prefix_to_remove[-1] != '.':
            prefix_to_remove += '.'
        self.keys = self.name.replace(prefix_to_remove, '').split('.')
        self.logger.debug('Keys for ListCut \'%s\': %s' % (self.name, self.keys))

    # Same as for DictCut
    def resetKeys(self):
        self.keys = self.name.split('.')
        self.logger.debug('Keys for ListCut \'%s\': %s' % (self.name, self.keys))

    # We first want to check if we need to add our cut to a nested ListCut
    def __addCut__toNestedListCut(self, cut):
        # Figure our which names in _LIST_CUTS our cut name matches (via regexes)
        matching_names = [name for name, regex in _LIST_CUTS.items() if re.search(regex, cut.name)]
        # If there are no matching names, we return False
        if matching_names != []:
            # Sort them by the number of periods present (i.e., depth)
            matching_names = sorted(matching_names, key = lambda string: string.count('.'))
            try:
                # The targeted name we want is the name we want is one level below the current level
                # Index of the name in matching_names
                targeted_index = matching_names.index(self.name) + 1
            except ValueError:
                # Shouldn't happen if everything is done right
                self.logger.error('ListCut \'%s\' could not be found in _LIST_CUTS -- this should not occur as the ListCut\'s name is derived from the keys of that dictionary!' % self.name)
                raise Exception
            # If we're targeting a nonexistent ListCut level, add the cut to the current one
            if targeted_index == len(matching_names):
                return False
            targeted_name = matching_names[targeted_index]
            # Try adding the cut to an existing ListCut
            try:
                list_cut = self[self.getChildCutNames().index(targeted_name)]
            except ValueError:
                # If the ListCut does not exist, create it and add the cut to it
                list_cut = ListCut(targeted_name, parent = self)
                print list_cut.parent
                self.addCut(list_cut)
            list_cut.addCut(cut)
            # This bypasses the rest of the treatment of DictCuts
            return True
        else:
            return False

    def addCut(self, cut):
        # Cut must be of type DictCut or ListCut
        if isinstance(cut, (DictCut, ListCut)):
            # Check if we have already added this cut at the present cut level
            try:
                i = self.getChildCutNames().index(cut.name)
                self.logger.warning('Trying to add %s \'%s\' to ListCut \'%s\' which already contains %s \'%s\' of the same name -- skipping.' % (cut.__class__.__name__, cut.name, self.name, (self.conditional_cuts + self.follower_cuts)[i].__class__.__name__, (self.conditional_cuts + self.follower_cuts)[i].name))
                return False
            except ValueError:
                pass
            # Reset our keys, as they may be reduced differently if this cut has gone through several nested addCut calls
            # Good to do anyways for safety (probably not even needed)
            # cut.resetKeys()
            # First check if we can add the cut to a nester ListCut (next bit of code is triggered if it doesn't have a list path in the cut's name or if we haven't handled this particular
            # list location in one of our input dicts)
            if not self.__addCut__toNestedListCut(cut):
                # For when we have type(cut) == DictCut
                if isinstance(cut, DictCut):
                    # If we have a nonzero pass status (0 being highest priority at a given ListCut level), allow self.pass_status to still be changed
                    if self.pass_status != 0:
                        # If the pass status is 0 for the current DictCut, immediately set self.pass_status = 0
                        if cut.pass_status == 0:
                            self.pass_status = cut.pass_status
                            # Update our apply function to require all items to pass
                            self.apply = self.__apply__PASSED
                            self.setPassReq = lambda list0: len(list0)
                        # If the (negative) pass status is greater (more restrictive, as we can fail fewer items) than the current pass status, update self.pass_status
                        elif cut.pass_status < 0 and self.pass_status < cut.pass_status:
                            self.pass_status = cut.pass_status
                            # Here we use the __FAILED apply call as we want to fail up to some maximum number of items
                            self.apply = self.__apply__FAILED
                        # If the (positive) pass status is greater (more restrictive, as we have to pass more items) than the current pass status, update self.pass_status
                        elif 0 < cut.pass_status and self.pass_status < cut.pass_status:
                            self.pass_status = cut.pass_status
                            self.apply = self.__apply__PASSED
                            self.setPassReq = lambda list0: self.pass_status
                        # Otherwise do nothing
                        else:
                            pass
                    # Append the cut to the appropriate list, depending on whether the cut is a conditional or a follower
                    if cut.conditional:
                        self.conditional_cuts.append(cut)
                        self.logger.debug('Added (conditional) DictCut \'%s\' to ListCut \'%s\'.' % (cut.name, self.name))
                    else:
                        self.follower_cuts.append(cut)
                        self.logger.debug('Added (follower) DictCut \'%s\' to ListCut \'%s\'.' % (cut.name, self.name))
                # For when we have type(cut) == ListCut
                # Here, we enforce ListCuts for be followers
                else:
                    self.follower_cuts.append(cut)
                    self.logger.debug('Added (follower) ListCut \'%s\' to ListCut \'%s\'.' % (cut.name, self.name))
                # Reduce the keys so we know how to properly access this value
                cut.reduceKeys(self.name)
            # Set the cut's parent to be the current object (for propagating logging)
            cut.addParent(self)
            return True
        else:
            self.logger.error('Trying to add cut of unknown type to ListCut \'%s\': type(cut) = %s' % (cut.name, self.name, cut.__class__.__name__))
            raise Exception

    # This function is applied for the case where pass_status > 0 (we want to see how many items pass)
    # --> pass_all := require every item in the list to pass
    def __apply__PASSED(self, dict0):
        list0 = reduce(lambda d, k: d.get(k) if d else None, self.keys, dict0)
        local_pass_status = 0
        pass_requirement = self.setPassReq(list0)
        # Iterate through our list
        for i, item in enumerate(list0):
            self.logger.debug('On item %s for ListCut \'%s\'.' % (i, self.name))
            # First check if the conditional pass passes
            conditional_pass = True
            conditional_pass = reduce(lambda conditional_pass, cut: conditional_pass & cut.apply(item) if conditional_pass else False, self.conditional_cuts, conditional_pass)
            if conditional_pass:
                # Now we see if the item passes our follower cuts and see how many items in the list pass
                local_pass_status += reduce(lambda follower_pass, cut: follower_pass & cut.apply(item) if follower_pass else False, self.follower_cuts, conditional_pass)
                self.logger.debug('Pass status: %s' % local_pass_status)
                # If we reach our pass requirement, return True for the list
                if local_pass_status == pass_requirement:
                    return True
            else:
                # Skip items that fail conditional cuts
                continue
        # If we get here, we never reach our pass requirement and return False
        return False

    # Same idea as above, but for pass_status < 0
    def __apply__FAILED(self, dict0):
        list0 = reduce(lambda d, k: d.get(k) if d else None, self.keys, dict0)
        local_fail_status = 0
        fail_requirement = self.pass_status
        for item in list0:
            self.logger.debug('On item %s for ListCut \'%s\'.' % (i, self.name))
            conditional_pass = True
            conditional_pass = reduce(lambda conditional_pass, cut: conditional_pass & cut.apply(item) if conditional_pass else False, self.conditional_cuts, conditional_pass)
            if conditional_pass:
                # Subtract the NOT result
                local_fail_status -= (not reduce(lambda follower_pass, cut: follower_pass & cut.apply(item) if follower_pass else False, self.follower_cuts, conditional_pass))
                self.logger.debug('Fail status: %s' % local_fail_status)
                # If we reach our fail status, return False
                if local_fail_status == fail_requirement:
                    return False
            else:
                continue
        # If we get here, return True (as we never reached our fail status)
        return True

#######################################################################################################################################################################################################

# ------------------------------------------------------------------------
# CutFlow object for containing/applying a collection of DictCuts/ListCuts
# ------------------------------------------------------------------------

# We define a CutFlow object which handles a collection of cuts
class CutFlow(object):

    __slots__ = ['name', 'cuts', 'parent', 'logger', 'iter_index']

    def __init__(self, name, cuts = [], parent = None):
        super(CutFlow, self).__init__()
        self.name       = name
        # Check for the parent and add it
        self.parent     = None
        self.logger     = _LOGGER_DEFAULT
        self.addParent(parent)
        self.logger.debug('Instantiated CutFlow \'%s\'.' % name)
        self.cuts       = []
        self.addCut(*cuts)
        self.iter_index = 0

    def __str__(self):
        return self.name

    def __len__(self):
        return len(self.cuts)

    def __getitem__(self, i):
        return self.cuts[i]

    def __iter__(self):
        self.iter_index = 0
        return self

    def __next__(self):
        if self.iter_index <= (len(self.cuts) - 1):
            cut = self.cuts[self.iter_index]
            self.iter_index += 1
            return cut
        else:
            raise StopIteration

    # Python 2 iterator fix, see: https://stackoverflow.com/questions/29578469/how-to-make-an-object-both-a-python2-and-python3-iterator
    next = __next__

    # Same as addCut
    def __call__(self, *args):
        self.addCut(*args)

    def getLongName(self):
        return 'CutFlow \'%s\'' % self.name

    def getChildCutNames(self):
        return [cut.name for cut in self]

    def addParent(self, parent):
        self.parent = parent
        # Use the parent's logger
        self.logger = parent.logger

    # Return the name of the cutflow and all contained cuts (with nice indenting)
    def representation(self, indent = 0):
        string = indent * ' ' + self.getLongName() + '\n'
        for i in range(len(self)):
            string += self[i].representation(indent = indent + 1)
        return string

    def show(self, indent = 1):
        self.logger.info('Printing CutFlow \'%s\':\n%s' % (self.name, self.representation(indent)))

    # We first want to check if we need to add our cut to a nested ListCut
    def __addCut__toNestedListCut(self, cut):
        # Figure our which names in _LIST_CUTS our cut name matches (via regexes)
        matching_names = [name for name, regex in _LIST_CUTS.items() if re.search(regex, cut.name)]
        # If there are no matching names, we return False
        if matching_names != []:
            # Filter matching_names to have only 1 period present
            matching_names = list(filter(lambda string: (string.count('.') == 0), matching_names))
            # The targeted name we want should be the only item in the list
            if len(matching_names) != 1:
                self.logger.error('%s \'%s\' should be added to a single nested ListCut, but its name was matched to >2 cuts in _LIST_CUTS each having zero \'.\'s: %s' % (cut.__class__.__name__, cut.name, matching_names))
                raise Exception
            else:
                targeted_name = matching_names[0]
            # Try adding the cut to an existing ListCut
            try:
                list_cut = self.cuts[self.getChildCutNames().index(targeted_name)]
            except ValueError:
                # If the ListCut does not exist, create it and add the cut to it
                list_cut = ListCut(targeted_name, parent = self)
                self.addCut(list_cut)
            list_cut.addCut(cut)
            # This bypasses the rest of the treatment of DictCuts
            return True
        else:
            return False

    # Add a cut(s) to our cutflow object
    def addCut(self, *args):
        for cut in args:
            # Cut must be of type DictCut or ListCut
            if isinstance(cut, (DictCut, ListCut)):
                # Check if we have already added this cut at the present cut level
                try:
                    i = self.getChildCutNames().index(cut.name)
                    self.logger.warning('Trying to add %s \'%s\' to CutFlow \'%s\' which already contains %s \'%s\' of the same name -- skipping.' % (cut.__class__.__name__, cut.name, self.name, self.cuts[i].__class__.__name__, self.cuts[i].name))
                    return False
                except ValueError:
                    pass
                # Reset our keys, as they may be reduced differently if this cut has gone through several nested addCut calls
                # Good to do anyways for safety (probably not even needed)
                # cut.resetKeys()
                # First check if we can add the cut to a nester ListCut (next bit of code is triggered if it doesn't have a list path in the cut's name or if we haven't handled this particular
                # list location in one of our input dicts)
                if not self.__addCut__toNestedListCut(cut):
                    # All cuts get appended to self.cuts
                    self.cuts.append(cut)
                    self.logger.info('Added %s \'%s\' to CutFlow \'%s\'.' % (cut.__class__.__name__, cut.name, self.name))
            else:
                self.logger.error('Trying to add cut \'%s\' of unknown type to CutFlow \'%s\': type(cut) = %s' % (cut.name, self.name, cut.__class__.__name__))
                raise Exception
            # Set the cut's parent to be the current object (for propagating logging)
            cut.addParent(self)
            return True

    # Apply the cutflow to a dict
    def apply(self, dict0):
        for cut in self.cuts:
            try:
                # If the dict fails the cut, return False
                if not cut.apply(dict0):
                    self.logger.debug('Dict \033[1m\033[91mfailed\033[0m at cut \'%s\'.' % cut.name)
                    return False
            # We also return False if we get an exception raised in fetching the dict's value
            except (ValueError, TypeError, KeyError) as e:
                self.logger.warning('Dict \033[1m\033[91mfailed\033[0m at cut \'%s\' due to %s: %s' % (cut.name, type(e).__name__, e))
                return False
        # If we get here, the component passed all cuts -- return True!
        self.logger.debug('Dict \033[1m\033[92mpassed\033[0m all cuts.')
        return True

    # Recursively fetch items in lists
    def __getValue__fromList(self, dict0, path_names):
        # Get the list pointed to by the first element of path_names
        list0 = reduce(lambda d, k: d.get(k) if d else None, path_names[0], dict0)
        # Remove element 0 from the other strings in the list and delete element 0 (we've already applied it)
        path_names = [name.replace(path_names[0], '') for name in path_names]
        del path_names[0]
        # If there is only one path left, return the object specified by that path
        if len(path_names) == 1:
            return [reduce(lambda d, k: d.get(k) if d else None, path_names[0], item) for item in list0]
        else:
            # Else apply the function again to the reduced dict
            return [self.__getValue__fromList(item, path_names) for item in list0]

    # Define a function for getting some value from a data dict (somewhat misplaced to put it in our cutflow object, but it could really be anywhere...)
    # NOTE: function is a function that would take the result of getValue and process it somehow (maybe more relevant if getValue would otherwise return e.g. a list)
    def getValue(self, dict0, path, function = None):
        keys = path.split('.')
        # Figure our which names in _LIST_CUTS matches our path (using the regexes)
        matching_names = [name for name, regex in _LIST_CUTS.items() if re.search(regex, path)]
        if matching == []:
            # Simple extraction, we can use reduce to directly get the value
            value = reduce(lambda d, k: d.get(k) if d else None, keys, dict0)
        else:
            # Else sort the matched names by number of '.'s
            matching_names = sorted(matching_names, key = lambda string: string.count('.'))
            # Recursively apply the below to get the value for every element of each list encountered
            # matching_name + [path] defines the paths through lists to the final dictionary item
            value = self.__getValue__fromList(dict0, matching_names + [path])
        self.logger.debug('Value for CutFlow \'%s\' (path = \'%s\'): %s' % (self.name, path, value))
        # If a function is specified, apply the function to the extracted result
        if function:
            value = function(value)
            self.logger.debug('Function return: %s' % value)
        # Return
        return value

    # If we are certain the value to be extracted follows only dictionary keys, then this function extracts the value much faster
    # keys := path.split('.') (i.e., list of dict keys)
    def getValueFast(self, dict0, keys, function = None):
        try:
            value = reduce(lambda d, k: d.get(k) if d else None, keys, dict0)
        except (ValueError, TypeError, KeyError) as e:
            self.logger.warning('Dict raised %s: %s' % (cut.name, type(e).__name__, e))
            return None
        if function:
            value = function(value)
        return value

#######################################################################################################################################################################################################

# ------------------------
# Reporting job definition
# ------------------------

# Define the fundamental reporting job class
# It should have its own logger so that we can save the actions performed (maybe we want to do this with the debug logger -- a lot of printout???)
class ReportingJob(object):

    ########################################################################################################################
    ### IMPORTANT: https://stackoverflow.com/questions/701802/how-do-i-execute-a-string-containing-python-code-in-python ###
    ########################################################################################################################

    __slots__ = ['logger', 'parent', 'name', 'session', 'data', 'breakdown', 'parameters', 'constrict', 'cutflow', 'iter_index']

    def __init__(self, name, session = None, data = [], defintion = None, breakdown = None, parameters = None, constrict = True, parent = None):
        super(ReportingJob, self).__init__()
        # Name of the job (should be unique):
        self.name = name
        # ITkPDSession (in case we need to send requests):
        self.session = session
        # Let's create our logger now (since other stuff in the init depends on the logger)
        self.logger = getLogger(name = name, show_in_console = True, sh_level = logging.INFO)
        self.logger.debug('Instantiated ReportingJob \'%s\'.' % name)
        # Then check for the parent (as we want the parent logger's handlers to the child, I think...?)
        self.parent = None
        self.addParent(parent)
        # Data to filter (list of dicts):
        self.data = []
        self.addData(data)
        # Breakdown to classify results by (string: '<key0>.<key1>.(...).<keyN>'):
        self.breakdowns = breakdown
        self.__checkBreakdown()
        # Parameters to extract for each dictionary
        self.parameters = parameters
        self.__checkParameters()
        # Constrict the data (reduce data by only selecting items which pass selection):
        self.constrict = constrict
        # Instantiated a cutflow for out object (every job gets their own cutflow, should be interacted with via the job itself):
        self.cutflow = CutFlow(name, parent = self) # We also make the parent of the cutflow the current object (for propagating logging)
        self.defineCutflow(definition)
        self.iter_index = 0

    def __str__(self):
        return self.name

    def __len__(self):
        return len(self.data)

    def __getitem__(self, i):
        return self.data[i]

    def __iter__(self):
        self.iter_index = 0
        return self

    def __next__(self):
        if self.iter_index <= (len(self.data) - 1):
            item = self.data[self.iter_index]
            self.iter_index += 1
            return item
        else:
            raise StopIteration

    # Python 2 iterator fix, see: https://stackoverflow.com/questions/29578469/how-to-make-an-object-both-a-python2-and-python3-iterator
    next = __next__

    # Same as addCut
    def __call__(self, *args):
        self.addCut(*args)

    def getLongName(self):
        return 'ReportingJob \'%s\'' % self.name

    def addParent(self, parent):
        if not isinstance(parent, MultiReportingJob):
            self.logger.error('Parent of ReportingJob \'%s\' must be of type \'MultiReportingJob\': type(parent) = %s' % (self.name, type(parent)))
            raise Exception
        self.parent = parent
        # Get the parent's session and logger handlers (tho I don't think we use the sessions anywhere in these classes)
        self.session = self.parent.session
        self.logger.handlers = self.parent.logger.handlers

    # Add an ITkPDSession to the object
    def addSession(self, session):
        self.session = session

    # Return the class's stored session
    def getSession(self, session):
        return self.session

    # Add a cut(s) (i.e., args) to the member cutflow
    def addCut(self, *args):
        self.cutflow.addCut(*args)

    # Just a fancier version of the addCut function, one can add a list of DictCuts or something like:
    # [
    #   (<args for DictCut #1>),
    #   (<args for DictCut #2>),
    #   ...
    #   (<args for DictCut #N>)
    # ]
    def defineCutflow(self, definition):
        if definition:
            if not isinstance(definition, (list, tuple)):
                self.logger.error('Cutflow definition for ReportingJob \'%s\' must be of type \'list\' or \'tuple\': type(definition) = %s' % (self.name, type(definition)))
                raise Exception
            for i, cut in enumerate(definition):
                if isinstance(cut, (list, tuple)):
                    dict_cut = DictCut(*cut, parent = self)
                    self.addCut(dict_cut)
                    name = cut[0]
                elif isinstance(cut, DictCut):
                    self.addCut(cut)
                    name = cut.name
                else:
                    self.logger.error('Cut (index = %s) in cutflow definition for ReportingJob \'%s\' must be of type \'list\' or \'tuple\' (if defining the arguments of a DictCut) or of type \'DictCut\': type(cut) = %s' % (i, self.name, type(cut)))
                    raise Exception
                self.logger.debug('Successfully added DictCut \'%s\' to ReportingJob \'%s\'.' % (name, self.name))

    # Add data to run over
    # If overwrite = True, we will set self.data = data, else we append to self.data
    def addData(self, data, overwrite = True):
        # Enforce the data to be a list
        if not isinstance(data, list):
            logger.error('Data added to ReportingJob \'%s\' must be of type \'list\': type(data) = %s' % (self.name, type(data)))
            raise Exception
        if overwrite:
            self.data = data
        else:
            self.data += data
        self.logger.debug('Added data to ReportingJob \'%s\': len(data) = %s' % (self.name, len(data)))

    # Return the data
    def getData(self):
        return self.data

    # Clear the stored data
    def clearData(self):
        del self.data
        self.data = []

    # Check our scheme for classifying list items (i.e., breakdown)
    # e.g.:
    # [
    #   'currentLocation.code'  # converted to (<value to extract>, <name given to this value in the results dictionary>) = ('currentLocation.code', 'currentLocation.code')
    #   ('type.code', 'type')   # extracts 'type.code', but names it simply 'type' in the results dictionary
    #   ...
    #   <and so on>
    # ]
    def __checkBreakdown(self):
        if self.breakdown is not None:
            # First check if the breakdown is only a string (i.e., classifying according according to only one key)
            if isinstance(self.breakdown, str):
                # Convert to a list
                self.breakdown = [(self.breakdown, self.breakdown)]
            # Else check if a list of strings
            elif isinstance(self.breakdown, (list, tuple)):
                for i, classifier in enumerate(self.breakdown):
                    if isinstance(classifier, str):
                        self.breakdown[i] = (classifier, classifier)
                    elif isinstance(classifier, (list, tuple)):
                        if not len(classifier) == 2:
                            self.logger.error('Elements in breakdown for ReportingJob \'%s\' of type \'list\' or \'tuple\' must be of length 2: len(breakdown[i = %s]) = %s' % (self.name, i, len(classifier)))
                            raise Exception
                    else:
                        self.logger.error('Elements in breakdown for ReportingJob \'%s\' must of type \'str\', \'list\', or \'tuple\': type(breakdown[i = %s]) = %s' % (self.name, i, type(classifier)))
                        raise Exception
                    self.logger.debug('Added breakdown on path \'%s\' (given name \'%s\' in results dict) to ReportingJob \'%s\'.' % (self.breakdown[i][0], self.breakdown[i][1], self.name))
            else:
                self.logger.error('Breakdown for ReportingJob \'%s\' must be of type \'str\' or list/tuple[\'str\']: type(breakdown) = %s' % (self.name, type(self.breakdown)))
                raise Exception

    # Check our scheme for fetching and processing item results (i.e., parameters)
    def __checkParameters(self):
        if self.parameters is not None:
            pass

    # Try to the guess the type of the items we are running over and return a function for identifying the current item
    # e.g., serial number, component code, shipment number, test run results id, etc.
    # May have to add additional handling here
    def __guessType(self, item):
        keys = item.keys()
        # I think only component dicts have key 'code' in the top level...
        if 'serialNumber' in keys or 'code' in keys:
            self.logging.debug('ReportingJob \'%s\' --> running on lists of components.' % self.name)
            def identify(item):
                # Not every component has a serial number...
                try:
                    return ('Component (SN = \'%s\')' % item['serialNumber'])
                # ...so we use the component code in these cases
                except KeyError:
                    return ('Component (code = \'%s\')' % item['code'])
        elif 'shipmentNumber' in keys:
            self.logging,debug('ReportingJob \'%s\' --> running on lists of shipments.' % self.name)
            def identify(item):
                return ('Shipment (number = \'%s\')' % item['shipmentNumber'])
        else:
            self.logging.debug('ReportingJob \'%s\' --> running on lists of test runs.' % self.name)
            def identify(item):
                return ('Test run (id = \'%s\')' % item['id'])
        return identify

    # Run the job!
    # This is the important function of the class, called on its own or by the parent MultiReportingJob
    def run(self, clear_data = True, pipeline_from = None):
        # Stamp the beginning of the job
        start_timestamp = strftime('%d.%m.%Y-%H:%M:%S')
        self.logger.info('ReportingJob \'%s\' start timestamp (DD.MM.YYYY-HH:MM:SS): %s' % (self.name, start_timestamp))
        if pipeline_from:
            self.logger.info('ReportingJob \'%s\' is seeded with the results of ReportingJob \'%s\'.' % (self.name, pipeline_from))
        # Our results dictionary (should we pickle it...??? is that the way to store things?)
        results =   {   '$PARAMETERS$': {   '$NAME$':               self.name,
                                            '$START-TIMESTAMP$':    start_timestamp,
                                            '$END-TIMESTAMP$':      '',
                                            '$DATA-SEED$':          '',             # Leave empty for now (could put doSomething call which seeded the data)
                                            '$PIPELINE$':           self.pipeline,
                                            '$PIPELINE-FROM':       pipeline_from,
                                            '$CUTFLOW$':            self.cutflow.representation(),
                                            '$BREAKDOWN-PATH$':     [classifier[0] for classifier in self.breakdown],
                                            '$BREAKDOWN-NAME$':     [classifier[1] for classifier in self.breakdown]
                                        },
                        '$DATA$':       {}
                    }
        self.logger.info('Printing cutflow for ReportingJob \'%s\':\n%s' % (self.name, self.cutflow.representation()))
        # Items which pass all cuts
        passed_items = []
        # Use the first item in the data list to guess the type of data (i.e., components, test run results, shipments, etc.)
        identify = self.__guessType(self.data[0])
        # Now begin the event loop
        for item in self.data:
            # Check if the item passing the selection
            if self.cutflow.apply(item):
                self.logger.info('%s \033[1m\033[92mpassed\033[0m all cuts.' % identify(item))
                passed_items.append(item)
            else:
                self.logger.info('%s \033[1m\033[91mfailed\033[0m one or more cuts.' % identify(item))
                pass
            if self.constrict:
                self.data = passed_items
        # Stamp the end of the job
        finish_timestamp = strftime('%d.%m.%Y-%H:%M:%S')
        self.logger.info('ReportingJob \'%s\' finish timestamp (DD.MM.YYYY-HH:MM:SS): %s' % (self.name, finish_timestamp))
        results['$PARAMETERS$']['$DATE-TIME_END$'] = finish_timestamp
        # Clear the data (for cleanup purposes -- we have a switch in case we want to pipe the data)
        if clear_data:
            self.clearData()

# Define a class for handling multiple ReportingJobs which share a dataset
class MultiReportingJob(object):

    __slots__ = ['name', 'session', 'data', 'jobs', 'pipeline', 'logger', 'iter_index']

    def __init__(self, name, session = None, data = [], jobs = [], pipeline = False):
        super(MultiReportingJob, self).__init__()
        # Name of MultiReportingJob (I don't think this is too important to set, probably for debugging it should be something descriptive):
        self.name = name
        # ITkPDSession (to be passed to children):
        self.session = session
        # The input data to be passed to the jobs:
        self.data = data
        # The list of jobs to be run by the class:
        self.jobs = jobs
        # If we are pipeing the data from one job to another:
        self.pipeline = pipeline
        # Get our logger (since other stuff in the init depends on the logger)
        self.logger = getLogger(name = name, show_in_console = True, sh_level = logging.INFO)
        self.iter_index = 0
        self.logger.debug('Instantiated MultiReportingJob \'%s\'.')

    def __str__(self):
        return self.name

    def __len__(self):
        return len(self.jobs)

    def __getitem__(self, i):
        return self.jobs[i]

    def __iter__(self):
        self.iter_index = 0
        return self

    def __next__(self):
        if self.iter_index <= (len(self.jobs) - 1):
            job = self.jobs[self.iter_index]
            self.iter_index += 1
            return job
        else:
            raise StopIteration

    # Python 2 iterator fix, see: https://stackoverflow.com/questions/29578469/how-to-make-an-object-both-a-python2-and-python3-iterator
    next = __next__

    # Same as addJob
    def __call__(self, *args):
        self.addJob(*args)

    # Add data to either the first child of the MultiReportingJob (if we want to pipe the results from one to the next) or add the same data to each job
    def addData(self, data, overwrite = True):
        # In case of a pipeline
        if self.pipeline:
            self.logger.debug('MultiReportingJob \'%s\' has defined as a pipeline -- adding data to MultiReportingJob[i = 0].')
            self.jobs[0].addData(data, overwrite)
        else:
            self.logger.debug('Adding data to each child of MultiReportingJob \'%s\'.' % self.name)
            for job in self.jobs:
                job.addData(data, overwrite)

    # Add ReportingJobs to out MultiReportingJob
    def addJob(self, *args):
        for i, job in enumerate(args):
            # Enforce all jobs to be of type 'ReportingJob'
            if not isinstance(job, ReportingJob):
                self.logger.error('Attempting to add argument that is not of type \'ReportingJob\' to MultiReportingJob \'%s\': type(args[i = %s]) = %s' % (self.name, i, type(job)))
                raise Exception
            # Update the child's ITkPDSession
            self.logger.debug('Passing the ITkPDSession instance owned by MultiReportingJob \'%s\' to ReportingJob \'%s\'.' % (self.name, job.name))
            job.addParent(self)
            self.jobs.append(job)
            self.logger.debug('Added ReportingJob \'%s\' to MultiReportingJob \'%s\'.' % (job.name, self.name))

    # Run all child jobs
    def run(self):
        self.logger.info('Running children of MultiReportingJob \'%s\'.' % self.name)
        for i, job in enumerate(self.jobs):
            # If we are running a pipeline, we don't want to clear the data at the end of run(...)
            job.run(clear_data = (not self.pipeline), pipeline_from = (self.jobs[i - 1].name if self.pipeline else None))
            # If we are running a pipeline, pipe the results from one job to another
            if self.pipeline:
                self.logger.info('Piping results from ReportingJob \'%s\' to seed ReportingJob \'%s\'.' % (job.name, self.jobs[i + 1].name))
                self.jobs[i + 1].addData(job.getData())
        self.logger.info('Finished running children of MultiReportingJob \'%s\'.' % self.name)

#######################################################################################################################################################################################################

# ---------------------------
# Main (for testing purposes)
# ---------------------------

if __name__ == '__main__':

    # Import and authenticate our session
    from itk_pdb.dbAccess import ITkPDSession
    session = ITkPDSession()
    session.authenticate()

    # Generate a list of components for testing
    print('**************************************************')
    print('DEBUG :: Fetching components.')
    components = session.doSomething('listComponents', 'GET', {'project': 'S', 'componentType': 'HYBRID'})
    components = session.doSomething('getComponentBulk', 'GET', {'component': [component['code'] for component in components]})
    print('DEBUG :: Finished fetching components.')

    # Define and run a sample job
    job = ReportingJob(name = 'test', data = components, constrict = True)
    print('DEBUG :: Length of components before running: %s\n' % len(job)),
    definition =    [   ('properties.code', '==', 'LOCALNAME'),                         # If a property's code is 'LOCALNAME' (i.e., this is a conditional cut)...
                        ('properties.value', 'RE', r'TO2R0_.*_VAN'),                    # ...then check if the value matches the regex 'TO2R0_.*_VAN' (i.e., this is a follower cut)
                        ('reworked', 'IN', [False, 'shiba_inu', 100]),                  # Check if reworked points to one of the items in the list
                        ('children.component', 'TRUE'),                                 # Check that at least one of the children are assembled
                        ('attachments.contentType', '==', 'application/octet-stream'),   # Check that at least one of the attachments have content type 'application/octet-stream'
                        ('attachments.contentType', '==', 'application/octet-stream') 
                    ]
    job.defineCutflow(definition)
    import time
    t0 = time.time()
    job.run(clear_data = False)
    tf = time.time() - t0
    print('DEBUG :: Time to finish job.run(...): %s s' % tf)
    # Should be 1
    print('DEBUG :: Length of components after running: %s' % len(job))
    print('**************************************************')
