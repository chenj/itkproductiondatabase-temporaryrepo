#!/usr/bin/env python
# ReportingClasses.py
# Created: 2019/04/01, Updated: 2019/06/12
# Contributing: MJB

import re, sys
from functools import reduce
from time import strftime
from requests.exceptions import RequestException

#######################################################################################################################################################################################################

class Colours():
    White       = '\033[95m'
    Blue        = '\033[94m'
    Green       = '\033[92m'
    Yellow      = '\033[93m'
    Red         = '\033[91m'
    End         = '\033[0m'
    Bold        = '\033[1m'
    Underline   = '\033[4m'

# Define some pretty printing functions
# Here, they are all included in a single class so we can implement inheritance
# Note: sys._getframe(1) is for fetching (the name of) the function calling the print statement
class Printer(object):

    def __init__(self):
        super(Printer, self).__init__()

    def INFO(self, string):
        print('\033[1m\033[97mINFO\033[0m in %s.%s(...) : %s' % (self.__class__.__name__, sys._getframe(1).f_code.co_name, string))

    def WARNING(self, string):
        print('\033[1m\033[93mWARNING\033[0m in %s.%s(...) : %s' % (self.__class__.__name__, sys._getframe(1).f_code.co_name, string))

    def ERROR(self, string):
        print('\033[1m\033[91mERROR\033[0m in %s.%s(...) : %s' % (self.__class__.__name__, sys._getframe(1).f_code.co_name, string))

    def BREAK(self, string):
        print('\033[1m\033[91mBREAK\033[0m in %s.%s(...) : %s' % (self.__class__.__name__, sys._getframe(1).f_code.co_name, string))
        raise Exception

#######################################################################################################################################################################################################

# (Very marginally!) modified function for printing the size of composite object
# Taken from: https://code.activestate.com/recipes/577504/
from sys import getsizeof
from itertools import chain
from collections import deque
try:
    from reprlib import repr
except ImportError:
    pass

def total_size(o, handlers = {}, verbose = False):
    """ Returns the approximate memory footprint an object and all of its contents.

    Automatically finds the contents of the following builtin containers and
    their subclasses:  tuple, list, deque, dict, set and frozenset.
    To search other containers, add handlers to iterate over their contents:

        handlers = {SomeContainerClass: iter,
                    OtherContainerClass: OtherContainerClass.get_elements}

    """
    dict_handler = lambda d: chain.from_iterable(d.items())
    all_handlers = {tuple: iter,
                    list: iter,
                    deque: iter,
                    dict: dict_handler,
                    set: iter,
                    frozenset: iter,
                   }
    all_handlers.update(handlers)     # user handlers take precedence
    seen = set()                      # track which object id's have already been seen
    default_size = getsizeof(0)       # estimate sizeof object without __sizeof__

    def sizeof(o):
        if id(o) in seen:       # do not double count the same object
            return 0
        seen.add(id(o))
        s = getsizeof(o, default_size)

        if verbose:
            print(s, type(o), repr(o))

        for typ, handler in all_handlers.items():
            if isinstance(o, typ):
                s += sum(map(sizeof, handler(o)))
                break
        return s

    return sizeof(o)

#######################################################################################################################################################################################################

# Define a cut class with a Cut.apply__BASE(...) function defined during instantiation
# apply method is define during instantiation, returns component's value for the cut and the pass status
# __dict__ slot is needed for dynamic self.apply (I think...)

# We also define a global list of allowed operators
# The individual operator cases must be handled in Cut.__init__
G_ALLOWED_OPERATORS = ['<', '<=', '=<', '>', '>=', '=>', '==', '=', '!=', 'IN', 'XIN', 'RE', 'TRUE', 'FALSE', 'DUMMY', 'FALSE']

class Cut(Printer):

    __slots__ = ['name', 'operator', 'cut_value', 'apply__BASE']

    def __init__(self, name, operator, cut_value = None):
        self.name       = name
        self.operator   = operator
        self.cut_value  = cut_value
        # Have to go through and manually add the filter functions associated with each operator
        # Maybe we could have G_ALLOWED_OPERATORS map operator keys to the functions, though then the cut value
        # would also need to be included as an argument...
        if operator == '<':
            self.apply__BASE = lambda value: (value < cut_value)
        elif operator == '<=' or self.operator == '=<':
            self.apply__BASE = lambda value: (value, value <= cut_value)
        elif operator == '>':
            self.apply__BASE = lambda value: (value, value > cut_value)
        elif operator == '>=' or self.operator == '=>':
            self.apply__BASE = lambda value: (value, value >= cut_value)
        elif operator == '==' or self.operator == '=':
            self.apply__BASE = lambda value: (value, value == cut_value)
        elif operator == '!=':
            self.apply__BASE = lambda value: (value, value != cut_value)
        elif operator == 'IN':
            self.apply__BASE = lambda value: (value, value in list(cut_value))
        elif operator == 'XIN':
            self.apply__BASE = lambda value: (value, value not in list(cut_value))
        elif operator == 'RE':
            self.apply__BASE = lambda value: (value, bool(re.search(str(cut_value), str(value))))
        elif operator == 'TRUE':
            self.apply__BASE = lambda value: (value, bool(value))
        elif operator == 'FALSE':
            self.apply__BASE = lambda value: (value, not bool(value))
        elif operator == 'DUMMY':
            self.apply__BASE = lambda value: (value, True)
        elif operator == 'XDUMMY':
            self.apply__BASE = lambda value: (value, False)
        else:
            # It's possible that the operator is in G_ALLOWED_OPERATORS but isn't handled above so we get to this point
            # Again, probably a better of handling things, but this works for now
            self.BREAK('Operator must be one of %s: operator = %s' % (G_ALLOWED_OPERATORS, operator))

    def __str__(self):
        return self.name

#######################################################################################################################################################################################################

# For lists of components from the ITkPD, we define a list of conditionals
# These are keys in the component dict which we want to apply cuts to (conditional cuts) before applying follower cuts
# --> We use regexes here for matching
# properties.code .id
G_CONDITIONALS = [r'$.*\.code', r'$.*\.id', r'$.*\.name']

# We define the keys in a general component dict which point to lists within the larger dictionary structure
G_LISTS =   {   'attachments':          r'^attachments\..*',
                'children':             r'^children\..*',
                'children.properties':  r'^children\.properties\..*',
                'comments':             r'^comments\..*',
                'grades':               r'^grades\..*',
                'parents':              r'^parents\..*',
                'properties':           r'^properties\..*',
                'stages':               r'^stages\..*',
                'tests':                r'^tests\..*',
                'tests.testRuns':       r'^tests\.testRuns\..*'
            }

# Now we define a class for applying a cut on a dictionary
# --> Inherits from Cut
class DictCut(Cut):

    __slots__ = ['pass_status', 'conditional', 'keys']

    def __init__(self, name, operator, cut_value, pass_status = 1):
        super(DictCut, self).__init__(name, operator, cut_value)
        if not isinstance(pass_status, int):
            self.BREAK('Pass status for cut \'%s\' must be an integer: pass_status = %s' % (self.name, pass_status))
        # Cuts have an int pass status that tells them whether it must pass for at least x (if x > 0) items, all items (if x == 0), or fail no more than |x| (if x < 0)
        self.pass_status    = pass_status
        # Determine if this cuts fits one of our conditional tags
        self.conditional    = bool(re.search(r'[' + r'|'.join(G_CONDITIONALS) + r']', self.name))
        # Keys that must be sequentially applied to the dictionary to get the value to cut one
        self.keys           = self.name.split('.')

    # We want to remove part of our set of keys if the cut is applied as part of a ListCut
    # --> Parent ListCut applies these already
    def reduceKeys(self, prefix_to_remove):
        if prefix_to_remove[-1] != '.':
            prefix_to_remove += '.'
        self.keys = self.name.replace(prefix_to_remove, '').split('.')

    # Reset the keys
    def resetKeys(self):
        self.keys = self.name.split('.')

    # Sequentially apply the keys to a dictionary to get the value to cut on, then applies Cut.apply(...)
    def apply(self, dict0):
        return super(DictCut, self).apply__BASE(reduce(lambda d, k: d.get(k) if d else None, self.keys, dict0))

# We define an object for applying DictCuts to lists
class ListCut(Printer):

    __slots__ = ['name', 'pass_status', 'keys', 'conditional_cuts', 'follower_cuts', 'apply', 'iter_index']

    def __init__(self, name, pass_status = 1):
        super(ListCut, self).__init__()
        self.name               = name
        self.pass_status        = pass_status
        self.keys               = self.name.split('.')
        # We have a cuts classified as conditional
        self.conditional_cuts   = []
        # ...and those classified as not conditional
        self.follower_cuts      = []
        # Default value for our apply(...) function
        self.apply              = lambda list0: self.__apply__PASSED(list0)
        self.iter_index         = 0
        self.verbose = True

    def __str__(self):
        return self.name

    def __len__(self):
        return len(self.conditional_cuts + self.follower_cuts)

    def __getitem__(self, i):
        return (self.conditional_cuts + self.follower_cuts)[i]

    def __iter__(self):
        self.iter_index = 0
        return self

    def __next__(self):
        if self.iter_index <= (len(self) - 1):
            cut = self[self.iter_index]
            self.iter_index += 1
            return cut
        else:
            raise StopIteration

    # Python 2 iterator fix, see: https://stackoverflow.com/questions/29578469/how-to-make-an-object-both-a-python2-and-python3-iterator
    next = __next__

    # Same as for DictCut
    def reduceKeys(self, prefix_to_remove):
        if prefix_to_remove[-1] != '.':
            prefix_to_remove += '.'
        self.keys = self.name.replace(prefix_to_remove, '').split('.')

    def resetKeys(self):
        self.keys = self.name.split('.')

    # We first want to check if we need to add our cut to a nested ListCut
    def __addCut__toNestedListCut(self, cut):
        # Figure our which names in G_LISTS our cut name matches
        matching_names = []
        # Get the matching names via the regexes
        for name, regex in G_LISTS.items():
            if re.search(regex, cut.name):
                matching_names.append(name)
        # If there are no matching names, we return False
        if matching_names != []:
            # Sort them by the number of periods present (i.e., depth)
            matching_names = sorted(matching_names, key = lambda string: len([0 for char in string if char == '.']))
            try:
                # The targeted name we want is the name we want is one level below the current level
                # Index of the name in matching_names
                targeted_index = matching_names.index(self.name) + 1
            except ValueError:
                # Shouldn't happen if everything is done right
                self.BREAK('ListCut \'%s\' could not be found in G_LISTS -- this should not occur as the ListCut\'s name is derived from the keys of that dictionary!' % self.name)
            # If we're targeting a nonexistent ListCut level, add the cut to the current one
            if targeted_index == len(matching_names):
                return False
            targeted_name = matching_names[targeted_index]
            # Try adding the cut to an existing ListCut
            added_to_existing_ListCut = False
            for cut_loaded in (self.conditional_cuts + self.follower_cuts):
                if cut_loaded.name == targeted_name:
                    cut_loaded.addCut(cut)
                    added_to_existing_ListCut = True
            # If the ListCut does not exist, create it and add the cut to it
            if not added_to_existing_ListCut:
                new_list_cut = ListCut(targeted_name)
                self.addCut(new_list_cut)
                new_list_cut.addCut(cut)
            # This bypasses the rest of the treatment of DictCuts
            return True
        else:
            return False

    def addCut(self, cut):
        # Cut must be of type DictCut or ListCut
        if isinstance(cut, (DictCut, ListCut)):
            # Check if we have already added this cut at the present cut level
            for cut_loaded in (self.conditional_cuts + self.follower_cuts):
                if cut.name == cut_loaded.name:
                     self.WARNING('Trying to add %s \'%s\' to %s \'%s\' which already contains %s \'%s\' of the same name -- skipping.' % (cut.__class__.__name__, cut.name, self.__class__.__name__, self.name, cut_loaded.__class__.__name__, cut_loaded.name))
                     return False
            # Reset our keys, as they may be reduced differently if this cut has gone through several nested addCut calls
            # Good to do anyways for safety (probably not even needed)
            cut.resetKeys()
            # First check if we can add the cut to a nester ListCut (next bit of code is triggered if it doesn't have a list path in the cut's name or if we haven't handled this particular
            # list location in one of our input dicts)
            if not self.__addCut__toNestedListCut(cut):
                # For when we have type(cut) == DictCut
                if isinstance(cut, DictCut):
                    # If we have a nonzero pass status (0 being highest priority at a given ListCut level), allow self.pass_status to still be changed
                    if self.pass_status != 0:
                        # If the pass status is 0 for the current DictCut, immediately set self.pass_status = 0
                        if cut.pass_status == 0:
                            self.pass_status = cut.pass_status
                            # Update our apply function to require all items to pass
                            self.apply = lambda dict0: self.__apply__PASSED(dict0, True)
                        # If the (negative) pass status is greater (more restrictive, as we can fail fewer items) than the current pass status, update self.pass_status
                        elif cut.pass_status < 0 and self.pass_status < cut.pass_status:
                            self.pass_status = cut.pass_status
                            # Here we use the __FAILED apply call as we want to fail up to some maximum number of items
                            self.apply = lambda dict0: self.__apply__FAILED(dict0)
                        # If the (positive) pass status is greater (more restrictive, as we have to pass more items) than the current pass status, update self.pass_status
                        elif 0 < cut.pass_status and self.pass_status < cut.pass_status:
                            self.pass_status = cut.pass_status
                            self.apply = lambda dict0: self.__apply__PASSED(dict0)
                        # Otherwise do nothing
                        else:
                            pass

                    # Append the cut to the appropriate list, depending on whether the cut is a conditional or a follower
                    if cut.conditional:
                        self.conditional_cuts.append(cut)
                    else:
                        self.follower_cuts.append(cut)
                # For when we have type(cut) == ListCut
                # Here, we enforce ListCuts for be followers
                else:
                    self.follower_cuts.append(cut)
                # Reduce the keys so we know how to properly access this value
                cut.reduceKeys(self.name)
            return True
        else:
            self.BREAK('Trying to add cut \'%s\' of unknown type to %s \'%s\': type = %s' % (cut.name, self.__class__.__name__, self.name, cut.__class__.__name__))

    # This function is applied for the case where pass_status > 0 (we want to see how many items pass)
    # --> pass_all := require every item in the list to pass
    def __apply__PASSED(self, dict0, pass_all = False):
        list0 = reduce(lambda d, k: d.get(k) if d else None, self.keys, dict0)
        local_pass_status = 0
        if pass_all:
            pass_requirement = len(list0)
        else:
            pass_requirement = self.pass_status
        # Iterate through our list
        for item in list0:
            # First check if the conditional pass passes
            conditional_pass = True
            conditional_pass = reduce(lambda conditional_pass, cut: conditional_pass & cut.apply(item)[1] if conditional_pass else False, self.conditional_cuts, conditional_pass)
            if conditional_pass:
                # Now we see if the item passes our follower cuts and see how many items in the list pass
                local_pass_status += reduce(lambda follower_pass, cut: follower_pass & cut.apply(item)[1] if follower_pass else False, self.follower_cuts, conditional_pass)
                # If we reach our pass requirement, return True for the lsit
                if local_pass_status == pass_requirement:
                    return('passed = %s' % local_pass_status, True)
            else:
                # Skip items that fail conditional cuts
                continue
        # If we get here, we never reach our pass requirement and return False
        return ('passed = %s' % local_pass_status, False)

    # Same idea as above, but for pass_status < 0
    def __apply__FAILED(self, dict0):
        list0 = reduce(lambda d, k: d.get(k) if d else None, self.keys, dict0)
        local_fail_status = 0
        fail_requirement = self.pass_status
        for item in list0:
            conditional_pass = True
            conditional_pass = reduce(lambda conditional_pass, cut: conditional_pass & cut.apply(item)[1] if conditional_pass else False, self.conditional_cuts, conditional_pass)
            if conditional_pass:
                # Subtract the NOT result
                local_fail_status -= (not reduce(lambda follower_pass, cut: follower_pass & cut.apply(item)[1] if follower_pass else False, self.follower_cuts, conditional_pass))
                # If we reach our fail status, return False
                if local_fail_status == fail_requirement:
                    return('failed = %s' % local_fail_status, False)
            else:
                continue
        # If we get here, return True (as we never reached our fail status)
        return ('failed = %s' % local_fail_status, True)

# We define a CutCollection object which handles a collection of Cuts (doesn't apply cuts just yet)
class CutFlow(Printer):

    __slots__ = ['name', 'cuts', 'iter_index', 'verbose']

    def __init__(self, name = ''):
        super(CutFlow, self).__init__()
        self.name = name
        self.cuts = []
        self.iter_index = 0
        self.verbose = 1

    def __str__(self):
        return self.name

    def __len__(self):
        return len(self.cuts)

    def __getitem__(self, i):
        return self.cuts[i]

    def __iter__(self):
        self.iter_index = 0
        return self

    def __next__(self):
        if self.iter_index <= (len(self.cuts) - 1):
            cut = self.cuts[self.iter_index]
            self.iter_index += 1
            return cut
        else:
            raise StopIteration

    # Python 2 iterator fix, see: https://stackoverflow.com/questions/29578469/how-to-make-an-object-both-a-python2-and-python3-iterator
    next = __next__

    # We first want to check if we need to add our cut to a nested ListCut
    def __addCut__toNestedListCut(self, cut):
        # Figure our which names in G_LISTS our cut name matches
        matching_names = []
        # Get the matching names via the regexes
        for name, regex in G_LISTS.items():
            if re.search(regex, cut.name):
                matching_names.append(name)
        # If there are no matching names, we return False
        if matching_names != []:
            # Filter matching_names to have only 1 period present
            matching_names = list(filter(lambda string: len([0 for char in string if char == '.']) == 0, matching_names))
            # The targeted name we want should be the only item in the list
            if len(matching_names) != 1:
                self.BREAK('%s \'%s\' should be added to a nested ListCut, but its name was matched to two cuts in G_LISTS each having one \'.\': %s' % (cut.__class__.__name__, cut.name, matching_names))
            else:
                targeted_name = matching_names[0]
            # Try adding the cut to an existing ListCut
            added_to_existing_ListCut = False
            for cut_loaded in (self.cuts):
                if cut_loaded.name == targeted_name:
                    cut_loaded.addCut(cut)
                    added_to_existing_ListCut = True
            # If the ListCut does not exist, create it and add the cut to it
            if not added_to_existing_ListCut:
                new_list_cut = ListCut(targeted_name)
                self.addCut(new_list_cut)
                new_list_cut.addCut(cut)
            # This bypasses the rest of the treatment of DictCuts
            return True
        else:
            return False

    def addCut(self, cut):
        # Cut must be of type DictCut or ListCut
        if isinstance(cut, (DictCut, ListCut)):
            # Check if we have already added this cut at the present cut level
            for cut_loaded in (self.cuts):
                if cut.name == cut_loaded.name:
                     self.WARNING('Trying to add %s \'%s\' to CutFlow which already contains %s \'%s\' of the same name -- skipping.' % (cut.__class__.__name__, cut.name, self.__class__.__name__, self.name, cut_loaded.__class__.__name__, cut_loaded.name))
                     return False
            # Reset our keys, as they may be reduced differently if this cut has gone through several nested addCut calls
            # Good to do anyways for safety (probably not even needed)
            cut.resetKeys()
            # First check if we can add the cut to a nester ListCut (next bit of code is triggered if it doesn't have a list path in the cut's name or if we haven't handled this particular
            # list location in one of our input dicts)
            if not self.__addCut__toNestedListCut(cut):
                # All cuts get appended to self.cuts
                self.cuts.append(cut)
            return True
        else:
            self.BREAK('Trying to add cut \'%s\' of unknown type to CutFlow: type = %s' % (cut.name, self.__class__.__name__, self.name, cut.__class__.__name__))

    def apply(self, dict0):
        for cut in self.cuts:
            try:
                result = cut.apply(dict0)
                if not result[1]:
                    if self.verbose:
                        self.INFO('Component \'%s\' \033[1m\033[91mfailed\033[0m at cut \'%s\' due to value: %s' % (dict0['code'], cut.name, result[0]))
                    return False
            except (ValueError, TypeError, KeyError) as e:
                if self.verbose:
                    self.WARNING('Component \'%s\' \033[1m\033[91mfailed\033[0m at cut \'%s\' due to %s: %s' % (dict0['code'], cut.name, type(e).__name__, e))
                return False
        if self.verbose:
            self.INFO('Component \'%s\' \033[1m\033[92mpassed\033[0m all cuts.' % dict0['code'])
        return True

#######################################################################################################################################################################################################

class ComponentListConfigParser(Printer):

    def __init__(self, verbose = True):
        super(ComponentListConfigParser, self).__init__()
        self.verbose = verbose

    # Define a function for parsing the
    def parse(self, config, fetch = True):
        if not (isinstance(config, list) or isinstance(config, tuple)):
            self.BREAK('Config must be of type \'list\' or \'tuple\': type = %s' % type(config))
        for i, cut in enumerate(config):
            if not (isinstance(cut, list) or isinstance(cut, tuple)):
                self.BREAK('Cut index = %s must be of type \'list\' or \'tuple\': type = %s' % (i, type(cut)))
            elif not (2 <= len(cut) and len(cut) <= 4):
                self.BREAK('Cut index = %s must satisfy 2 <= length <= 4: length = %s ' % (i, len(cut)))
            elif not isinstance(cut[0], str):
                self.BREAK('Element 0 (\'name\') of cut index = %s must be of type \'str\': value = %s (type = %s)' % (i, cut[0], type(cut)))
            if len(cut) == 2:
                if isinstance(cut[1], list):
                    operator = 'IN'
                else:
                    operator = '=='
                config[i] = (cut[0], operator, cut[1])
        cutflow = CutFlow()
        listComponents_kwargs = {}
        for i, cut in enumerate(config):
            if fetch and cut[0] in ['project.code', 'componentType.code', 'subproject.code', 'type.code', 'currentStage.code']:
                if cut[0] == 'project.code':
                    allowed_projects = ['S', 'P', 'CE', 'CM']
                    if cut[2] not in allowed_projects:
                        self.BREAK('Cut name \'project.code\' must be one of %s: \'project.code\' = \'%s\'' % (allowed_projects, cut[2]))
                if isinstance(cut[2], list) and not cut[1] == 'IN':
                    self.BREAK('Cut name \'%s\' (index = %s) must have operator \'IN\' if cut_value is of type \'list\': operator = \'%s\'' % (cut[0], i, cut[1]))
                if not isinstance(cut[2], list) and not cut[1] == '==':
                    self.BREAK('Cut name \'%s\' (index = %s) must have operator \'==\' if cut_value is NOT of type \'list\': operator = \'%s\'' % (cut[0], i, cut[1]))
                if self.verbose:
                    self.INFO('Adding key \'%s\' with value \'%s\' for API request \'listComponents\'.' % (cut[0].split('.')[0], cut[2]))
                listComponents_kwargs[cut[0].split('.')[0]] = cut[2]
            else:
                if self.verbose:
                    self.INFO('Adding cut on \'%s\' with operator \'%s\' and cut value \'%s\'.' % (cut[0], cut[1], cut[2]))
                cutflow.addCut(DictCut(*cut))
        if fetch:
            project = listComponents_kwargs.get('project', None)
            if project is None:
                self.BREAK('Config must have key \'project\': \'project\' = %s' % project)
            componentType = listComponents_kwargs.get('componentType', None)
            if componentType in [None, []]:
                self.BREAK('Config must have key \'componentType\' and it cannot map to an empty list: \'componentType\' = %s' %  componentType)
        if self.verbose:
            self.INFO('Config successfully parsed!')
        return cutflow, listComponents_kwargs

class ComponentList(Printer):

    __slots__ = ['ITkPDSession', 'components', 'verbose', 'CutsConfig', 'iter_index', 'passCuts', 'fetch', 'filter', 'fetchComponentsDetailed', 'fetchTestsDetailed',
                    'returnNewComponentList', 'returnSizeOfComponents', 'setITkPDSession']

    def __init__(self, ITkPDSession = None, components = [], verbose = False):
        super(ComponentList, self).__init__()
        self.ITkPDSession   = ITkPDSession
        self.components     = components
        self.verbose        = verbose
        self.CutsConfig     = ComponentListConfigParser(verbose = self.verbose)
        self.iter_index     = 0

    def __getitem__(self, i):
        return self.components[i]

    def __len__(self):
        return len(self.components)

    def __iter__(self):
        self.iter_index = 0
        return self

    def __next__(self):
        if self.iter_index <= (len(self.components) - 1):
            component = self.components[self.iter_index]
            self.iter_index += 1
            return component
        else:
            raise StopIteration

    # Python 2 iterator fix, see: https://stackoverflow.com/questions/29578469/how-to-make-an-object-both-a-python2-and-python3-iterator
    next = __next__

    def clear(self):
        self.iter_index = 0

    def __del__(self):
        self.ITkPDSession = None
        self.components = []
        self.verbose = False
        self.clear()

    def fetch(self, config, fetch_full = False, actually_reduce_list = True):
        self.clear()
        cutflow, listComponents_kwargs = self.CutsConfig.parse(config, fetch = True)
        if fetch_full:
            self.components = 10000 * self.ITkPDSession.doSomething('getComponentBulk', 'GET', {'component': [component['code'] for component in self.ITkPDSession.doSomething('listComponents', 'GET', listComponents_kwargs)]})
        else:
            self.components = self.ITkPDSession.doSomething('listComponents', 'GET', listComponents_kwargs)
            print(len(self.components))
        if len(cutflow) == 0:
            if self.verbose:
                self.INFO('No additional cuts to apply on top of the API request!')
        else:
            import time
            t = time.time()
            if actually_reduce_list:
                self.components = [component for component in self.components if cutflow.apply(component)]
                print(time.time() - t)
                return len(self.components)
            else:
                return len([0 for component in self.components if cutflow.apply(component)])

    def filter(self, config, actually_reduce_list = True):
        self.clear()
        cutflow, listComponents_kwargs = self.CutsConfig.parse(config, fetch = False)
        if actually_reduce_list:
            self.components = [component for component in self.components if cutflow.apply(component)]
            return len(self.components)
        else:
            return len([0 for component in self.components if cutflow.apply(component)])

    def fetchComponentsDetailed(self, keys_to_add = [-1]):
        if not isinstance(keys_to_add, list):
            self.BREAK('keys_to_add must be of type \'list\': type = %s' % type(keys_to_add))
        for i, key in enumerate(keys_to_add):
            if not (isinstance(key, str) or keys_to_add == [-1]):
                self.BREAK('keys_to_add[index = %s] must be of type \'str\' or == [-1] to select all keys: type = %s' % (i, type(key)))
        if len(self.components) == 0:
            if self.verbose:
                self.WARNING('Length of self.components == 0 -- nothing to do so returning.')
            return
        components_detailed = self.ITkPDSession.doSomething('getComponentBulk', 'GET', {'component': [component['code'] for component in self.components]})
        if len(components_detailed) != len(self.components):
            self.BREAK('Length of detailed list doesn\'t equal length of self.components: len(components_detailed) = %s, len(self.components) = %s' % (len(components_detailed), len(self.components)))
        if keys_to_add == [-1]:
            self.components = components_detailed
        else:
            for i, component in components_detailed:
                for key in keys_to_add:
                    try:
                        self.components[i][key] = component[key]
                    except (ValueError, TypeError, KeyError) as e:
                        if self.verbose:
                            self.INFO('Component \'%s\' did not have detailed information added for key \'%s\' due to %s: %s' % (component['code'], key, type(e).__name__, e))
                        continue

    def fetchTestsDetailed(self, tests_to_fetch = [-1], keys_to_add = [-1]):
        if not isinstance(tests_to_fetch, list):
            self.BREAK('tests_to_fetch must be of type \'list\': type = %s' % type(tests_to_fetch))
        if not isinstance(keys_to_add, list):
            self.BREAK('keys_to_add must be of type \'list\': type = %s' % type(keys_to_add))
        for i, test in enumerate(tests_to_fetch):
            if not (isinstance(test, str) or tests_to_fetch == [-1]):
                self.BREAK('tests_to_fetch[index = %s] must be of type \'str\' or == [-1] to select all test types: type = %s' % (i, type(test)))
        for i, key in enumerate(keys_to_add):
            if not (isinstance(key, str) or keys_to_add == [-1]):
                self.BREAK('keys_to_add[index = %s] must be of type \'str\' == [-1] to select all keys associated with the selected test type(s): type = %s' % (i, type(keys)))
        if len(self.components) == 0:
            if self.verbose:
                self.WARNING('Length of self.components == 0 -- nothing to do so returning.')
            return
        if not self.components[0].get('tests', None):
            self.BREAK('Key \'tests\' is not in self.components[index == 0] -- did you forget to run command self.fetch(...) with fetch_full ==  True?')
        tests_detailed_index = 0
        if tests_to_fetch == [-1]:
            tests_detailed, error_map = self.ITkPDSession.doSomething('getTestRunBulk', 'GET', {'testRun': [test['id'] for component in self.components for test in component['tests']]}, return_error_map_too = True)
            tests_not_obtained = error_map['testRunsNotObtained']
            if len(tests_detailed) == 0:
                if self.verbose:
                    self.WARNING('No test runs were obtained from the database -- returning.')
                    # WARNING('ComponentList.fetchTestsDetailed(...) : Test runs not obtained: %s' % tests_not_obtained)
                return
            if keys_to_add == [-1]:
                for i in range(len(self.components)):
                    for j in range(len(self.components[i]['tests'])):
                        if self.components[i]['tests'][j]['id'] == tests_detailed[tests_detailed_index]['id']:
                            self.components[i]['tests'][j] = tests_detailed[tests_detailed_index]
                            tests_detailed_index += 1
                        elif self.components[i]['tests'][j]['id'] in tests_not_obtained:
                            if self.verbose:
                                self.WARNING('Test run id \'%s\' for component \'%s\' could not be obtained -- skipping.' % (self.components[i]['tests'][j]['id'], self.components[i]['code']))
                            continue
            else:
                for i in range(len(self.components)):
                    for j in range(len(self.components[i]['tests'])):
                        if self.components[i]['tests'][j]['id'] == tests_detailed[tests_detailed_index]['id']:
                            for key in keys_to_add:
                                try:
                                    self.components[i]['tests'][j][key] = tests_detailed[tests_detailed_index][key]
                                except (ValueError, TypeError, KeyError) as e:
                                    if self.verbose:
                                        self.WARNING('Key \'%s\' for test id \'%s\' could not be added to component \'%s\' due to %s: %s' % (key, self.components[i]['tests'][j]['id'], self.components[i]['code'], type(e).__name__, e))
                                    continue
                            tests_detailed_index += 1
                        elif self.components[i]['tests'][j]['id'] in tests_not_obtained:
                            if self.verbose:
                                self.WARNING('Test run id \'%s\' for component \'%s\' could not be obtained -- skipping.' % (self.components[i]['tests'][j]['id'], self.components[i]['code']))
                            continue
        else:
            tests_detailed, error_map = self.ITkPDSession.doSomething('getTestRunBulk', 'GET', {'testRun': [test['id'] for component in self.components for test in component['tests'] if test['code'] in tests_to_fetch]}, return_error_map_too = True)
            tests_not_obtained = error_map['testRunsNotObtained']
            if len(tests_detailed) == 0:
                if self.verbose:
                    self.WARNING('No test runs were obtained from the database -- returning.')
                return
            if keys_to_add == [-1]:
                for i in range(len(self.components)):
                    for j in range(len(self.components[i]['tests'])):
                        if self.components[i]['tests'][j]['code'] in tests_to_fetch:
                            if self.components[i]['tests'][j]['id'] == tests_detailed[tests_detailed_index]['id']:
                                self.components[i]['tests'][j] = tests_detailed[tests_detailed_index]
                                tests_detailed_index += 1
                            elif self.components[i]['tests'][j]['id'] in tests_not_obtained:
                                if self.verbose:
                                    self.WARNING('Test run id \'%s\' for component \'%s\' could not be obtained -- skipping.' % (self.components[i]['tests'][j]['id'], self.components[i]['code']))
                                continue
                        else:
                            continue
            else:
                for i in range(len(self.components)):
                    for j in range(len(self.components[i]['tests'])):
                        if self.components[i]['tests'][j]['code'] in tests_to_fetch:
                            if self.components[i]['tests'][j]['id'] == tests_detailed[tests_detailed_index]['id']:
                                for key in keys_to_add:
                                    try:
                                        self.components[i]['tests'][j][key] = tests_detailed[tests_detailed_index][key]
                                    except (ValueError, TypeError, KeyError) as e:
                                        if self.verbose:
                                            self.WARNING('Key \'%s\' for test id \'%s\' could not be added to component \'%s\' due to %s: %s' % (key, self.components[i]['tests'][j]['id'], self.components[i]['code'], type(e).__name__, e))
                                        continue
                                tests_detailed_index += 1
                            elif self.components[i]['tests'][j]['id'] in tests_not_obtained:
                                if self.verbose:
                                    self.WARNING('Test run id \'%s\' for component \'%s\' could not be obtained -- skipping.' % (self.components[i]['tests'][j]['id'], self.components[i]['code']))
                                continue
                        else:
                            continue

    def returnNewComponentList(self):
        return ComponentList(ITkPDSession = self.ITkPDSession, components = self.components, verbose = self.verbose)

    def returnSizeOfComponents(self):
        size = total_size(self.components) / 1000
        if self.verbose:
            self.INFO('Size of self.components = %s MB.' % size)
        return size

    def setITkPDSession(self, ITkPDSession):
        self.ITkPDSession = ITkPDSession

g_ALLOWED_BREAKDOWNS = ['componentType', 'currentLocation', 'currentStage', 'institution', 'project', 'subproject', 'type']

class ReportingJob(Printer):

    __slots__ = ['ITkPDSession', 'name', 'job_definition', 'breakdown', 'verbose', 'ComponentList', 'commands', 'setITkPDSession', 'run']

    def __init__(self, name, job_definition, breakdown = {'top_code': 'institution', 'accepted_codes': [-1]}, ITkPDSession = None, verbose = False):
        super(ReportingJob, self).__init__()
        self.name           = name
        self.job_definition = job_definition
        self.breakdown      = breakdown
        self.ITkPDSession   = ITkPDSession
        self.verbose        = verbose
        self.ComponentList  = ComponentList(ITkPDSession = self.ITkPDSession, verbose = self.verbose)
        self.commands       = { 'fetch':                    lambda kwargs: self.ComponentList.fetch(actually_reduce_list = True, **kwargs),
                                'filter':                   lambda kwargs: self.ComponentList.filter(actually_reduce_list = True, **kwargs),
                                'fetchComponentsDetailed':  lambda kwargs: self.ComponentList.fetchComponentsDetailed(**kwargs),
                                'fetchTestsDetailed':       lambda kwargs: self.ComponentList.fetchTestsDetailed(**kwargs)   }
        if not (isinstance(self.job_definition, list) or isinstance(self.job_definition, tuple)):
            self.BREAK('Job definition for job name \'%s\' must be of type \'list\' or \'tuple\': type = %s' % (self.name, type(self.job_definition)))
        for i, job in enumerate(self.job_definition):
            if not isinstance(job, dict):
                self.BREAK('Task index = %s in job definition must be of \'dict\': type = %s' % (i, type(job)))
            elif not (job.keys() == ['command', 'kwargs'] or job.keys() == ['kwargs', 'command']):
                self.BREAK('Task index = %s in job definition  must have keys (and only keys) \'command\' and \'kwargs\': keys = %s' % (i, job.keys()))
        if not isinstance(self.breakdown, dict):
            self.BREAK('Breakdown must be of type \'dict\': type = %s' % type(self.breakdown))
        elif not (self.breakdown.keys() == ['top_code', 'accepted_codes'] or self.breakdown.keys() == ['accepted_codes', 'top_code']):
            self.BREAK('Breakdown must have keys (and only keys) \'top_code\' and \'accepted_codes\': keys = %s' % self.breakdown.keys())
        elif self.breakdown['top_code'] not in g_ALLOWED_BREAKDOWNS:
            self.BREAK('breakdown[\'top_code\'] must be one of %s: \'top_code\' = %s' % (g_ALLOWED_BREAKDOWNS ,self.breakdown['top_code']))
        elif not (isinstance(self.breakdown['accepted_codes'], list) or isinstance(self.breakdown['accepted_codes'], tuple)):
            self.BREAK('accepted_codes must be of type \'list\' or \'tuple\': type = %s' % type(self.breakdown['accepted_codes']))
        elif self.breakdown['accepted_codes'] != [-1]:
            for i, code in enumerate(self.breakdown['accepted_codes']):
                if not isinstance(code, str):
                    self.BREAK('Code index = %s in breakdown[\'accepted_codes\'] must be of type \'str\' (alternatively, breakdown[\'accepted_codes\'] can == [-1] to accept all codes): type = %s' % (i, type(code)))

    def setITkPDSession(self, ITkPDSession):
        self.ITkPDSession = ITkPDSession
        self.ComponentList.setITkPDSession(ITkPDSession)

    def run(self, verbose = False):
        results = {'$PARAMETERS$': {'$DATE-TIME_START$': strftime('%Y/%m/%d-%H:%M:%S'), '$DATE-TIME_END$': '', '$JOB_DEFINITION$': self.job_definition, '$BREAKDOWN$': self.breakdown}, '$DATA$': {}}
        top_code = self.breakdown['top_code']
        accepted_codes = self.breakdown['accepted_codes']
        if verbose or self.verbose:
            self.INFO('Running job with name \'%s\'.' % self.name)
            self.INFO('Start date-time = \'%s\'.' % results['$PARAMETERS$']['$DATE-TIME_START$'])
            self.INFO('Using top_code = \'%s\' with accepted_codes = %s.' % (top_code, accepted_codes))
        #     INFO('ReportingJob.run(...) : Fetching list of codes associated with top_code and consistent with accepted_codes.')
        # if top_code == 'componentType':
        #     for job in self.job_definition:
        #         if job['command'] == 'fetch' and 'project.code' in [key[0] for key in job['kwargs']['config'].keys()]:
        #             project = job['kwargs']['project']
        for job in self.job_definition:
            if verbose or self.verbose:
                self.INFO('Running command \'%s\' with kwargs = %s.' % (job['command'], job['kwargs']))
            self.commands[job['command']](job['kwargs'])
            if verbose or self.verbose:
                self.INFO('Finished command \'%s\'.' % job['command'])
        if verbose or self.verbose:
            self.INFO('Finished running commands in job definition.')
            self.INFO('Calculating counts according to breakdown.')
        if accepted_codes == [-1]:
            for component in self.ComponentList:
                code = component[top_code]['code']
                try:
                    results['$DATA$'][code] += 1
                except KeyError:
                    results['$DATA$'][code] = 1
        else:
            for component in self.ComponentList:
                code = component[top_code]['code']
                if code in accepted_codes:
                    try:
                        results[code] += 1
                    except KeyError:
                        results[code] = 1
        results['$PARAMETERS$']['$DATE-TIME_END$'] = strftime('%Y/%m/%d-%H:%M:%S')
        if verbose or self.verbose:
            self.INFO('Finished calculating counts.')
            self.INFO('Summary:')
            for key in [key for key in results['$DATA$'].keys() if key != '$PARAMETERS$']:
                print('    %s = %s' % (key, results['$DATA$'][key]))
            self.INFO('Finished job with name \'%s\'.' % self.name)
            self.INFO('End date-time = \'%s\'.' % results['$PARAMETERS$']['$DATE-TIME_END$'])
        return results

if __name__ == '__main__':

    # Import and authenticate our session
    from itk_pdb.dbAccess import ITkPDSession
    session = ITkPDSession()
    session.authenticate()

    job = ReportingJob('test', [{'command': 'fetch', 'kwargs': {'fetch_full': True, 'config': [('institution.code', 'UT'), ('project.code', 'S'), ('componentType.code', 'HYBRID'), ('properties.code', 'LOCALNAME'), ('properties.value', 'RE', 'TO2R0_.*_VAN'), ('children.component.code', 'TRUE', None), ('children.properties.code', 'POSITION')]}}], ITkPDSession = session, verbose = True)
    # job = ReportingJob('test', [{'command': 'fetch', 'kwargs': {'fetch_full': True, 'config': [('project.code', 'S'), ('componentType.code', 'HYBRID'), ('currentStage.code', 'COMPLETED')]}}, {'command': 'filter', 'kwargs': {'config': [('tests.code', 'RESPONSE_CURVE'), ('tests.testRuns.passed', True)]}}], ITkPDSession = session, verbose = True)
    job.run()
