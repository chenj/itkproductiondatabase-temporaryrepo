#!/usr/bin/env python
# ReportingJobs.py
# Created: 2019/05/05, Updated: 2019/07/04
# Contributing: MJB

if __name__ == '__main__':
    from __path__ import updatePath
    updatePath()

import sys
from itk_pdb.ReportingClasses import ReportingJob, MultiReportingJob

jobs = {}
jobs['weekly'] = []
jobs['monthly'] = []

jobs['weekly'].append   (   ReportingJob    (   name        =   'STRIPS_MODULES_LOADED',
                                                data        =   session.doSomething('listComponents', 'GET', {'project': 'S', 'componentType': 'MODULE', 'currentStage': 'COMPLETED'}),
                                                definition  =   [   ('assembled', 'TRUE'),
                                                                    ('completed', 'TRUE'),
                                                                    ('trashed', 'FALSE')
                                                                ],
                                                breakdown   =   [   'currentLocation.code',
                                                                    'type.code'
                                                                ]
                                            )
                        )

def main(args):
    from itk_pdb.dbAccess import ITkPDSession
    global session
    session = ITkPDSession()
    session.authenticate()
    for key in args.jobs:
        if key not in jobs.keys():
            print('ReportingJobs.py :: Unknown key \'%s\' -- skipping.' % key)
            continue
        for job in jobs[key]:
            job.setITkPDSession(session)
            job.run()

if __name__ == '__main__':

        import argparse
        parser = argparse.ArgumentParser(description = 'ITkPD reporting code', formatter_class = argparse.ArgumentDefaultsHelpFormatter)
        parser.add_argument('--jobs', dest = 'jobs', type = str, nargs = '*', help = 'Key(s) for the list(s) of jobs to be run')
        args = parser.parse_args()

        sys.exit(main(args))
