#Uploads Generic Sensor Data
#Created Jun 10 2019
#Written by Nashad Rahman rahman.176@osu.edu
#GoBucks

if __name__ == '__main__':
    from __path__ import updatePath

    updatePath()

from itk_pdb.dbAccess import ITkPDSession

import sys
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QLabel, QCheckBox, QWidget, QApplication, QPushButton, QLineEdit, QFileDialog,\
    QInputDialog, QMessageBox, QVBoxLayout, QTabWidget, QHBoxLayout, QGridLayout, QPlainTextEdit
from PyQt5.QtCore import QSize

import re
import argparse
import os



##Globals##
ignoreBlanks = False
delimiter = [": ", "= "]

session = ITkPDSession()
session.authenticate()


##############GUI STUFF##############

class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = 'Upload Data to Database'
        self.left = 100
        self.top = 100
        self.width = 700
        self.height = 350

        self.initUI()

        self.table_widget = MyTableWidget(self)
        self.setCentralWidget(self.table_widget)

    def initUI(self):

        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.show()


# Makes Seperate Tabs and objects within tabs
class MyTableWidget(QWidget):

    def __init__(self, parent):

        global delimiter

        super(QWidget, self).__init__(parent)
        self.layout = QGridLayout()
        self.selectedFile = "No file or directory selected"

        # Initialize tab screen
        self.tabs = QTabWidget()
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tabs.resize(300, 200)

        # Add tabs
        self.tabs.addTab(self.tab1, "Upload")
        self.tabs.addTab(self.tab2, "Options")

        # Create first tab
        self.tab1.layout = QGridLayout()

        self.tab1.layout.setColumnStretch(0, 1)
        self.tab1.layout.setColumnStretch(4, 1)
        self.tab1.layout.setColumnStretch(2, 10)
        self.tab1.layout.setRowStretch(0, 1)
        self.tab1.layout.setRowStretch(3, 1)


        ####Tab 1####

        # Layout Formatting
        self.tab1.layout = QGridLayout()

        self.tab1.layout.setColumnStretch(0, 1)
        self.tab1.layout.setColumnStretch(4, 1)
        self.tab1.layout.setColumnStretch(2, 10)
        self.tab1.layout.setRowStretch(0, 1)
        self.tab1.layout.setRowStretch(3, 1)

        # File Name Label (not input)
        self.nameLabel = QLabel(self)
        self.nameLabel.setText('File or Directory Name:')
        self.tab1.layout.addWidget(self.nameLabel,1,1)
        self.nameLabel.setMaximumSize(150, 15)

        # File input text
        self.line = QLineEdit(self)
        self.line.setText(self.selectedFile)
        self.tab1.layout.addWidget(self.line,1,2)
        self.line.move(100, 50)
        self.line.setMinimumSize(550, 30)

        # Select File Button
        self.fileSelect = QPushButton('Select File', self)
        self.fileSelect.setToolTip('Open window to select file location')
        self.fileSelect.clicked.connect(self.file_on_click)
        self.fileSelect.setMaximumSize(100, 30)
        self.tab1.layout.addWidget(self.fileSelect,1,3)
        self.tab1.setLayout(self.tab1.layout)

        # Final Upload Button
        self.uploadFiles = QPushButton('Upload', self)
        self.uploadFiles.setToolTip('Uploads selected file to database')
        self.uploadFiles.move(300, 170)
        self.uploadFiles.setMaximumSize(100, 30)
        self.uploadFiles.clicked.connect(self.upload_on_click)
        self.tab1.layout.addWidget(self.uploadFiles,3,2, QtCore.Qt.AlignCenter)


        ####Tab 2####
        self.tab2.layout = QGridLayout()

        # Ignore Blanks Checkbox
        self.b = QCheckBox("Ignore Blanks", self)
        self.b.stateChanged.connect(self.ignoreBlanksClickBox, 1, 1)
        self.b.move(280, 130)
        self.tab2.layout.addWidget(self.b)

        # Delimitter Label (not input)
        self.delimiterLabel = QLabel(self)
        self.delimiterLabel.setText('Delimitters:')
        self.tab2.layout.addWidget(self.delimiterLabel, 1, 0)
        self.delimiterLabel.setMaximumSize(100, 10)

        # Set a delimiter
        self.delimiterText = QLineEdit(self)
        self.delimiterText.setText(str(delimiter))
        self.tab2.layout.addWidget(self.delimiterText, 1, 2)
        self.delimiterText.setMinimumSize(550, 30)

        # Default Values Label (not input)
        self.defaultValuesText = QLabel(self)
        self.defaultValuesText.setText('Default Values (NA):')
        self.tab2.layout.addWidget(self.defaultValuesText, 2, 0)

        # Set Default Values Text
        self.defaultValuesText = QPlainTextEdit(self)
        self.tab2.layout.addWidget(self.defaultValuesText, 2, 2)
        self.defaultValuesText.setMinimumSize(550, 30)

        # Override Values Label (not input)
        self.overrideValues = QLabel(self)
        self.overrideValues.setText('Override Values (NA):')
        self.tab2.layout.addWidget(self.overrideValues, 3, 0)

        # Set override values
        self.overrideValues = QPlainTextEdit(self)
        self.tab2.layout.addWidget(self.overrideValues, 3, 2)
        self.overrideValues.setMinimumSize(550, 30)



        # Add tabs to widget
        self.tab1.setLayout(self.tab1.layout)
        self.tab2.setLayout(self.tab2.layout)

        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

    def ignoreBlanksClickBox(self, state):

        global ignoreBlanks

        if state == QtCore.Qt.Checked:
            ignoreBlanks = True

        else:
            ignoreBlanks = False


    def file_on_click(self):

        self.selectedFile = self.openFileNameDialog()

        self.line.setText(str(self.selectedFile))

        self.show()


    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "All Files (*);;Data Files (*.dat)", options=options)

        return (fileName)


    def upload_on_click(self):

        global delimiter

        delimiter = eval(str(self.delimiterText.text()))

        passFileOrFolder(str(self.line.text()), self.defaultValuesText.toPlainText(), self.overrideValues.toPlainText())



def YesOrNo(text): ##Easily calls and returns y/n

    dec = YesNo()
    dec.initUI(text)
    dec2 = dec.getDec()

    if dec2:
        return True
    else:
        return False

class YesNo(QWidget): ##Recycled for every y/n prompt


    def __init__(self):
        super().__init__()
        self.title = "Prompt"
        self.left = 10
        self.top = 10
        self.width = 320
        self.height = 200
        self.decision = False



    def initUI(self, text):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        buttonReply = QMessageBox.question(self, 'PyQt5 message', text,
                                           QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if buttonReply == QMessageBox.Yes:
            self.decision =  True

        else:
            self.decision = False


        self.show()

    def getDec(self):
        return self.decision

    def __repr__(self):
        return repr(self.decision)

    def __str__(self):
        return self.decision


def textInput(text): # Easily calls and returns text

    dec = TextInput()
    dec.initUI(text)
    dec2 = dec.userInput

    return dec2


class TextInput(QWidget): # Recycled for every text prompt

    def __init__(self):
        super().__init__()
        self.title = 'Request input'
        self.userInput = "notset"

    def initUI(self, text):
        self.setWindowTitle(self.title)
        self.setGeometry(10, 10, 640, 480)
        self.getText(text)
        self.show()

    def getText(self, propmtText):
        text, okPressed = QInputDialog.getText(self, "Get text", propmtText, QLineEdit.Normal, "")
        if okPressed and text != '':
            self.userInput = text


class Prompt(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'Upload Data to Database'
        self.left = 10
        self.top = 10
        self.width = 500
        self.height = 250
        self.selectedFile = 0
        self.initUI()


def clearDict(dictionary):                  # Clear the Dto sample acquired from the database (First step after getting Dto)
    for key, value in dictionary.items():

        if isinstance(value, list):
            dictionary[key] = []

        elif isinstance(value, dict):
            clearDict(value)

        else:
            dictionary[key] = "notset"


def setInfo(dicti, gkey, ginfo):            # Loops through an input dictionary, finding keys that match gkey, replacing its value with ginfo (Second step)
    for key, value in dicti.items():

        if isinstance(value, dict):
            setInfo(value, gkey, ginfo)

        else:
            if key == gkey:
                dicti[key] = ginfo


def getInfo(dicti, gkey):                   # Loops through an input dictionary, finding keys that match gkey, returns value
    for key, value in dicti.items():

        if isinstance(value, (dict,list)):
            return getInfo(value, gkey)

        else:
            if key == gkey:
                return value
            else: print("no value found")


def check_notset(info, inputLocation):      # Searches to see if any spaces have not been filled in, prompts user to manually input (Final Step)

    for key, value in info.items():

        if "notset" == info[key]:
            return False

        elif isinstance(value, dict):
            search_notset(value, inputLocation)

        return True


def search_notset(info, inputLocation):     # Searches to see if any spaces have not been filled in, prompts user to manually input (Final Step)

    for key, value in info.items():

        if "notset" == info[key]:
            dec = False
            dec = YesOrNo("WARNING: No value for \"" + key + "\" found in \n\n" + inputLocation + "\n\nWould you like to continue?")

            if not dec:
                exit(1)
            elif dec:
                dec = False
                dec = YesOrNo("Would you like to manually input a value?")

                if not dec:
                    continue
                elif dec:

                    manualInput = textInput("Enter value for " + key + ": ")

                    if str(manualInput).upper() == "TRUE":
                        manualInput = True
                    if str(manualInput).upper() == "FALSE":
                        manualInput = False

                    info[key] = manualInput


        elif isinstance(value, dict):
            search_notset(value, inputLocation)


def passFileOrFolder(inputLocation, defaultVal, overrideVal):

    if os.path.isdir(inputLocation):

        dec = False
        dec = YesOrNo("\nInput is a directory, please be sure only data files are present.\n\nWould you like to continue? (y/n) ")

        if not dec:
            return

        elif dec:

            for filename in os.listdir(inputLocation):  ##Cycles through all files in the directory

                DataUpload(inputLocation + filename, defaultVal, overrideVal)

    elif os.path.isfile(inputLocation):
        DataUpload(inputLocation, defaultVal, overrideVal)
    else:
        dec = False
        dec = YesOrNo("Invalid input location.\n\n Would you like to try again?")

        if dec:
            return
        if not dec:
            exit(0)


class dataToPass:
    def __init__(self): ##Initializes data with empty dictionary
        self.py_dict = {}


    def getDto(self, session, code, componentType, project):

        try:
            dtoString = session.doSomething('generateTestTypeDtoSample', 'GET', {'code': code, 'componentType': componentType, 'project': project, 'requiredOnly': False})

        except:
            dec = True
            dec = YesOrNo("INVALID INFORMATION TO GET DTO!\n\nWould you like to try again?")
            if dec:
                return
            else:
                exit(1)


        self.py_dict = dtoString["dtoSample"]

        clearDict(self.py_dict)
        #print(str(self.py_dict))

    ##Allow function to easily set values in dictionary, can be reused by other code
    def setBasic(self, pName, pValue):
        self.py_dict[pName] = pValue

    def setProperties(self, pName, pValue):
        self.py_dict["properties"][pName] = pValue

    def setResults(self, pName, pValue):
        self.py_dict["results"][pName] = pValue

    def addData(self, dName, dValue):
        self.py_dict["results"][dName].append(dValue)


    ##Uploads data to Itk DB
    def upload(self, session):

        try:

            #print(str(self.py_dict))
            session.doSomething('uploadTestRunResults', 'POST', data=self.py_dict)
            dec = False
            dec = YesOrNo("UPLOAD SUCCESSFUL! \n\n Would you like to upload another?") ##Comment Out Later CHANGE
            if not dec:
                exit(0) #This causes issues CHANGE
        except:
            dec = False
            dec = YesOrNo("UPLOAD FAILED \n\n Would you like to try again?")
            if not dec:
                exit(0)


def DataUpload(inputLocation, defaultVal, overrideVal):

    global delimiter

    data = dataToPass()

    defList = defaultVal.split("\n")
    ovrList = overrideVal.split("\n")
    #print(str(defList))


    try:

        with open(inputLocation) as f:
            fileData = f.read().splitlines()
    except:
        dec = False
        dec = YesOrNo("INVALID INPUT LOCATION \n\n Would you like to try again?")
        if not dec:
            exit(0)
        elif dec:
            return

    all = defList + fileData + ovrList
    allExceptOvr = defList + fileData

    for line in all:

        if "project: " in line:
            project = line[9:]
        if "componentType: " in line:
            componentType = line[15:]
        if "testType: " in line:
            testType = line[10:]

    try:
        inputLocation + project + componentType + testType
    except:

        dec = False
        dec = YesOrNo("Not enough info to get Dto for " + inputLocation + "\n\nWould you like to manually input values?")

        if not dec:
            return
        elif dec:
            try: project
            except: project = textInput("Enter project (S or P): ")

            try: componentType
            except: componentType = textInput("Enter component type (e.g. SENSOR): ")

            try: testType
            except: testType = textInput("Enter test type (e.g. SENSOR_STRIP_TEST): ")

    try:
        data.getDto(session, testType, componentType, project)
    except:
        dec = False
        dec = YesOrNo("Unable to get Dto from Itk PDB, please check that PROJECT, COMPONENT TYPE, and TEST TYPE math database exactly.\n\nWould you like to try again?")
        if not dec:
            exit(0)

    lineIsData = False                  # Changes to true once data is detected

    for line in allExceptOvr:

        for delim in delimiter:

            pos = line.find(delim)      # Information will be extracted if a colon is present

            if (pos != -1) :

                info_desc = line[:(pos)]
                extracted = line[(pos + 2):]

                #print(str(getInfo(data.py_dict, info_desc)) + str(info_desc))

                setInfo(data.py_dict, info_desc, extracted)

            # Add data to array once data header is reached
            if lineIsData:

                lineData = line.split()

                for m , title in enumerate(dataTitles):

                   data.addData(str(title), lineData[m])

            # Check if line is a data header and if so add data titles
            try:
                if line[0] == "%": #Indicates that the following lines are data and the current one is data

                    lineData = line.split()

                    dataTitles = []

                    for string in lineData:

                        try:
                            dataTitles.append((re.findall("%%(.*)/%", string)[0]))
                        except:
                            continue

                    lineIsData = True
            except:
                continue

        for line in ovrList:

            for delim in delimiter:

                pos = line.find(delim)  # Information will be extracted if a colon is present

                if (pos != -1):
                    info_desc = line[:(pos)]
                    extracted = line[(pos + 2):]

                    # print(str(getInfo(data.py_dict, info_desc)) + str(info_desc))

                    setInfo(data.py_dict, info_desc, extracted)


    if not ignoreBlanks:
        search_notset(data.py_dict, inputLocation)

    data.upload(session)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()

    sys.exit(app.exec_())