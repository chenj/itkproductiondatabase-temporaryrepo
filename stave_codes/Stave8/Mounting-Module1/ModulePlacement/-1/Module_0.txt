[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "1/23/2019 4:23:11 PM0.000000 "
Notes_Ideal = ""

[CornerA]
X_Ideal = -385.062190
Y_Ideal = -33.683498
Z_Ideal = 16.000000

[CornerB]
X_Ideal = -289.669775
Y_Ideal = -31.151937
Z_Ideal = 16.000000

[CornerC]
X_Ideal = -292.227971
Y_Ideal = 65.244123
Z_Ideal = 16.000000

[CornerD]
X_Ideal = -387.620385    
Y_Ideal = 62.712563    
Z_Ideal = 16.000000    