[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "1/23/2019 4:23:11 PM0.000000 "
Notes_Ideal = ""

[CornerA]
X_Ideal = -91.062231
Y_Ideal = -33.527044
Z_Ideal = 16.000000

[CornerB]
X_Ideal = 4.330183
Y_Ideal = -30.995484
Z_Ideal = 16.000000

[CornerC]
X_Ideal = 1.771988
Y_Ideal = 65.400577
Z_Ideal = 16.000000

[CornerD]
X_Ideal = -93.620427    
Y_Ideal = 62.869016    
Z_Ideal = 16.000000    