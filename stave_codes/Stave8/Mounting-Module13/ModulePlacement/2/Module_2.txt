[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "5/16/2019 10:29:42 AM, Temperature =  "
Notes_Ideal = ""

[CornerA]
X_Ideal = 662.329267
Y_Ideal = -33.667781
Z_Ideal = 16.274842

[CornerB]
X_Ideal = 757.724344
Y_Ideal = -31.238641
Z_Ideal = 16.274842

[CornerC]
X_Ideal = 755.269646
Y_Ideal = 65.160111
Z_Ideal = 16.274842

[CornerD]
X_Ideal = 659.874569    
Y_Ideal = 62.730970    
Z_Ideal = 16.274842    