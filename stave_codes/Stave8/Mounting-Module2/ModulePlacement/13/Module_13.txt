[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "1/31/2019 9:39:05 AM0.000000 "
Notes_Ideal = ""

[CornerA]
X_Ideal = -582.689340
Y_Ideal = -33.640714
Z_Ideal = 16.217782

[CornerB]
X_Ideal = -487.297007
Y_Ideal = -31.106107
Z_Ideal = 16.217782

[CornerC]
X_Ideal = -489.858281
Y_Ideal = 65.289872
Z_Ideal = 16.217782

[CornerD]
X_Ideal = -585.250614    
Y_Ideal = 62.755265    
Z_Ideal = 16.217782    