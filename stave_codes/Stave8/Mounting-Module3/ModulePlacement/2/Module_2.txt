[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "3/6/2019 10:27:17 AM0.000000 "
Notes_Ideal = ""

[CornerA]
X_Ideal = 495.428168
Y_Ideal = -33.286080
Z_Ideal = 16.273467

[CornerB]
X_Ideal = 590.823464
Y_Ideal = -30.865557
Z_Ideal = 16.273467

[CornerC]
X_Ideal = 588.377474
Y_Ideal = 65.533416
Z_Ideal = 16.273467

[CornerD]
X_Ideal = 492.982178    
Y_Ideal = 63.112893    
Z_Ideal = 16.273467    