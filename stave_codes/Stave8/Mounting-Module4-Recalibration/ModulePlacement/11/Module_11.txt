[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "4/19/2019 3:21:25 PM, Temperature =  "
Notes_Ideal = ""

[CornerA]
X_Ideal = -384.559154
Y_Ideal = -32.561331
Z_Ideal = 16.281705

[CornerB]
X_Ideal = -289.163900
Y_Ideal = -30.139138
Z_Ideal = 16.281705

[CornerC]
X_Ideal = -291.611577
Y_Ideal = 66.259793
Z_Ideal = 16.281705

[CornerD]
X_Ideal = -387.006831    
Y_Ideal = 63.837600    
Z_Ideal = 16.281705    