[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "4/19/2019 3:45:13 PM, Temperature =  "
Notes_Ideal = ""

[CornerA]
X_Ideal = 497.438314
Y_Ideal = -33.049254
Z_Ideal = 16.281705

[CornerB]
X_Ideal = 592.833678
Y_Ideal = -30.631439
Z_Ideal = 16.281705

[CornerC]
X_Ideal = 590.390425
Y_Ideal = 65.767603
Z_Ideal = 16.281705

[CornerD]
X_Ideal = 494.995060    
Y_Ideal = 63.349788    
Z_Ideal = 16.281705    