[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "4/19/2019 3:45:13 PM, Temperature =  "
Notes_Ideal = ""

[CornerA]
X_Ideal = 399.438335
Y_Ideal = -32.984553
Z_Ideal = 16.281705

[CornerB]
X_Ideal = 494.833700
Y_Ideal = -30.566738
Z_Ideal = 16.281705

[CornerC]
X_Ideal = 492.390447
Y_Ideal = 65.832305
Z_Ideal = 16.281705

[CornerD]
X_Ideal = 396.995082    
Y_Ideal = 63.414490    
Z_Ideal = 16.281705    