[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "5/7/2019 10:43:50 AM, Temperature =  "
Notes_Ideal = ""

[CornerA]
X_Ideal = 889.203864
Y_Ideal = -33.087593
Z_Ideal = 16.416577

[CornerB]
X_Ideal = 984.598592
Y_Ideal = -30.644781
Z_Ideal = 16.416577

[CornerC]
X_Ideal = 982.130078
Y_Ideal = 65.753618
Z_Ideal = 16.416577

[CornerD]
X_Ideal = 886.735350    
Y_Ideal = 63.310806    
Z_Ideal = 16.416577    