[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "5/7/2019 10:43:51 AM, Temperature =  "
Notes_Ideal = ""

[CornerA]
X_Ideal = 301.203910
Y_Ideal = -32.853462
Z_Ideal = 16.416577

[CornerB]
X_Ideal = 396.598638
Y_Ideal = -30.410650
Z_Ideal = 16.416577

[CornerC]
X_Ideal = 394.130125
Y_Ideal = 65.987749
Z_Ideal = 16.416577

[CornerD]
X_Ideal = 298.735397    
Y_Ideal = 63.544937    
Z_Ideal = 16.416577    