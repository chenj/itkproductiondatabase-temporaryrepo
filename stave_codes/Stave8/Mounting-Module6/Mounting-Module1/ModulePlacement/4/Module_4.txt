[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "1/23/2019 4:23:11 PM0.000000 "
Notes_Ideal = ""

[CornerA]
X_Ideal = 202.937727
Y_Ideal = -33.370591
Z_Ideal = 16.000000

[CornerB]
X_Ideal = 298.330141
Y_Ideal = -30.839031
Z_Ideal = 16.000000

[CornerC]
X_Ideal = 295.771946
Y_Ideal = 65.557030
Z_Ideal = 16.000000

[CornerD]
X_Ideal = 200.379532    
Y_Ideal = 63.025470    
Z_Ideal = 16.000000    