[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "1/23/2019 4:23:11 PM0.000000 "
Notes_Ideal = ""

[CornerA]
X_Ideal = 6.937755
Y_Ideal = -33.474893
Z_Ideal = 16.000000

[CornerB]
X_Ideal = 102.330169
Y_Ideal = -30.943333
Z_Ideal = 16.000000

[CornerC]
X_Ideal = 99.771974
Y_Ideal = 65.452728
Z_Ideal = 16.000000

[CornerD]
X_Ideal = 4.379559    
Y_Ideal = 62.921167    
Z_Ideal = 16.000000    