[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "1/31/2019 9:39:05 AM0.000000 "
Notes_Ideal = ""

[CornerA]
X_Ideal = -288.689387
Y_Ideal = -33.474872
Z_Ideal = 16.217782

[CornerB]
X_Ideal = -193.297054
Y_Ideal = -30.940265
Z_Ideal = 16.217782

[CornerC]
X_Ideal = -195.858328
Y_Ideal = 65.455714
Z_Ideal = 16.217782

[CornerD]
X_Ideal = -291.250661    
Y_Ideal = 62.921108    
Z_Ideal = 16.217782    