[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "1/31/2019 9:39:05 AM0.000000 "
Notes_Ideal = ""

[CornerA]
X_Ideal = -484.689356
Y_Ideal = -33.585433
Z_Ideal = 16.217782

[CornerB]
X_Ideal = -389.297023
Y_Ideal = -31.050826
Z_Ideal = 16.217782

[CornerC]
X_Ideal = -391.858297
Y_Ideal = 65.345153
Z_Ideal = 16.217782

[CornerD]
X_Ideal = -487.250630    
Y_Ideal = 62.810546    
Z_Ideal = 16.217782    