[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "1/31/2019 9:39:05 AM0.000000 "
Notes_Ideal = ""

[CornerA]
X_Ideal = 397.310504
Y_Ideal = -33.087906
Z_Ideal = 16.217782

[CornerB]
X_Ideal = 492.702837
Y_Ideal = -30.553299
Z_Ideal = 16.217782

[CornerC]
X_Ideal = 490.141563
Y_Ideal = 65.842680
Z_Ideal = 16.217782

[CornerD]
X_Ideal = 394.749230    
Y_Ideal = 63.308073    
Z_Ideal = 16.217782    