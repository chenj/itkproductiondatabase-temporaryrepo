[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "3/6/2019 10:27:17 AM0.000000 "
Notes_Ideal = ""

[CornerA]
X_Ideal = -582.571617
Y_Ideal = -32.604963
Z_Ideal = 16.273467

[CornerB]
X_Ideal = -487.176321
Y_Ideal = -30.184440
Z_Ideal = 16.273467

[CornerC]
X_Ideal = -489.622311
Y_Ideal = 66.214533
Z_Ideal = 16.273467

[CornerD]
X_Ideal = -585.017607    
Y_Ideal = 63.794010    
Z_Ideal = 16.273467    