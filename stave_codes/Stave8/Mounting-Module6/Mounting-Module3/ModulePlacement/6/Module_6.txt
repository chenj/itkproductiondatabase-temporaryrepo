[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "3/6/2019 10:27:17 AM0.000000 "
Notes_Ideal = ""

[CornerA]
X_Ideal = 103.428246
Y_Ideal = -33.038401
Z_Ideal = 16.273467

[CornerB]
X_Ideal = 198.823542
Y_Ideal = -30.617878
Z_Ideal = 16.273467

[CornerC]
X_Ideal = 196.377552
Y_Ideal = 65.781095
Z_Ideal = 16.273467

[CornerD]
X_Ideal = 100.982256    
Y_Ideal = 63.360572    
Z_Ideal = 16.273467    