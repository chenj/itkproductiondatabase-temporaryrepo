[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "4/19/2019 3:21:25 PM, Temperature =  "
Notes_Ideal = ""

[CornerA]
X_Ideal = -286.559172
Y_Ideal = -32.621535
Z_Ideal = 16.281705

[CornerB]
X_Ideal = -191.163918
Y_Ideal = -30.199342
Z_Ideal = 16.281705

[CornerC]
X_Ideal = -193.611596
Y_Ideal = 66.199589
Z_Ideal = 16.281705

[CornerD]
X_Ideal = -289.006850    
Y_Ideal = 63.777395    
Z_Ideal = 16.281705    