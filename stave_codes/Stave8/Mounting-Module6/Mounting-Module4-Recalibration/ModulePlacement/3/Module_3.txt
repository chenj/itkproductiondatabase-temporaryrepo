[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "4/19/2019 3:21:25 PM, Temperature =  "
Notes_Ideal = ""

[CornerA]
X_Ideal = 399.440698
Y_Ideal = -33.042963
Z_Ideal = 16.281705

[CornerB]
X_Ideal = 494.835952
Y_Ideal = -30.620770
Z_Ideal = 16.281705

[CornerC]
X_Ideal = 492.388275
Y_Ideal = 65.778160
Z_Ideal = 16.281705

[CornerD]
X_Ideal = 396.993021    
Y_Ideal = 63.355967    
Z_Ideal = 16.281705    