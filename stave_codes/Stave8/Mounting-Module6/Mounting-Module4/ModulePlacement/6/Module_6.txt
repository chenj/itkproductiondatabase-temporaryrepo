[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "4/19/2019 3:45:13 PM, Temperature =  "
Notes_Ideal = ""

[CornerA]
X_Ideal = 105.438399
Y_Ideal = -32.790447
Z_Ideal = 16.281705

[CornerB]
X_Ideal = 200.833764
Y_Ideal = -30.372632
Z_Ideal = 16.281705

[CornerC]
X_Ideal = 198.390511
Y_Ideal = 66.026410
Z_Ideal = 16.281705

[CornerD]
X_Ideal = 102.995146    
Y_Ideal = 63.608595    
Z_Ideal = 16.281705    