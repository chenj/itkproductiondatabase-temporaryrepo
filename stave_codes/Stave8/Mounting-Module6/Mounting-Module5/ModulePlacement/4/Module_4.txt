[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "5/7/2019 10:43:50 AM, Temperature =  "
Notes_Ideal = ""

[CornerA]
X_Ideal = 595.203887
Y_Ideal = -32.970527
Z_Ideal = 16.416577

[CornerB]
X_Ideal = 690.598615
Y_Ideal = -30.527715
Z_Ideal = 16.416577

[CornerC]
X_Ideal = 688.130101
Y_Ideal = 65.870684
Z_Ideal = 16.416577

[CornerD]
X_Ideal = 592.735373    
Y_Ideal = 63.427872    
Z_Ideal = 16.416577    