[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "5/15/2019 10:20:27 AM, Temperature =  "
Notes_Ideal = ""

[CornerA]
X_Ideal = 486.394869
Y_Ideal = -33.528443
Z_Ideal = 16.292980

[CornerB]
X_Ideal = 581.789931
Y_Ideal = -31.098706
Z_Ideal = 16.292980

[CornerC]
X_Ideal = 579.334631
Y_Ideal = 65.300030
Z_Ideal = 16.292980

[CornerD]
X_Ideal = 483.939569    
Y_Ideal = 62.870294    
Z_Ideal = 16.292980    