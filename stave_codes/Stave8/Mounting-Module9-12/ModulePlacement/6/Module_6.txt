[General]
ModuleID = "Test"
FiducialMark = "Mark E (Slim/17)"
Date_Ideal = "5/15/2019 10:20:27 AM, Temperature =  "
Notes_Ideal = ""

[CornerA]
X_Ideal = 290.394897
Y_Ideal = -33.423533
Z_Ideal = 16.292980

[CornerB]
X_Ideal = 385.789959
Y_Ideal = -30.993796
Z_Ideal = 16.292980

[CornerC]
X_Ideal = 383.334659
Y_Ideal = 65.404940
Z_Ideal = 16.292980

[CornerD]
X_Ideal = 287.939597    
Y_Ideal = 62.975204    
Z_Ideal = 16.292980    